function [ptri,tri] = getMesh(pboundary, sizeIm, hmax, meshtype)
% 
% construct mesh. either inner to the boundary, or outer, or full
% 
% output:
% ptri -    triangulation points
% tri -     triangulation indeces
% 
% input:
% pboundary -   boundary points
% sizeIm -      image size
% hmax -        maximum triangle side length
% meshtype -    'inner'|'outer'|full
% 

options.output = false;

if strcmpi(meshtype, 'outer') || strcmpi(meshtype, 'full')
    P = [pboundary; 1 1; 1 sizeIm(1); sizeIm(2) sizeIm(1); sizeIm(2) 1];
    edge = [1:size(pboundary,1); [2:size(pboundary,1) 1]]';
    E = [edge; size(edge,1)+[1 2; 2 3; 3 4; 4 1]];
    
    hdata.hmax = hmax;
    [ptriO,triO] = mesh2d(P, E, hdata, options);
    
    if strcmpi(meshtype, 'outer')
        ptri = ptriO;
        tri = triO;
        return
    end
end

if strcmpi(meshtype, 'inner') || strcmpi(meshtype, 'full')
    hdata.hmax = hmax;
    E = [1:size(pboundary,1); [2:size(pboundary,1) 1]]';
    [ptriI,triI] = mesh2d(pboundary, E, hdata, options);
    
    if strcmpi(meshtype, 'inner')
        ptri = ptriI;
        tri = triI;
        return
    end
    tri = [triO; triI + size(ptriO,1)];
    ptri = [ptriO; ptriI];
end

% [~,node_boundary,~] = intersect(ptriI, pboundary, 'rows');
% ptriI(node_boundary,:) = [];
% triI(node_boundary,:) = [];


% figure('Name','Mesh')
% hold on;
% plot(ptriO(:,1),ptriO(:,2),'b.','markersize',1)
% plot(ptriI(:,1),ptriI(:,2),'ro')
% patch('faces',triO,'vertices',ptriO,'facecolor','w','edgecolor', 'b');
% patch('faces',triI,'vertices',ptriI,'facecolor','w','edgecolor', 'c');
% patch('faces',tri,'vertices',ptri,'facecolor','w','edgecolor', 'm');




end