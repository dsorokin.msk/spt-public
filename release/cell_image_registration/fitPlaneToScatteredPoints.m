function [zq] = fitPlaneToScatteredPoints(x, y, z, xq, yq)
% 
% fits plane to points x,y,z
% returns the values evaluated in points xq, yq
% 

% Assuming that yourData is N-by-3 numeric array
N = length(x);
x = x(:);
y = y(:);
z = z(:);

B = [ones(N,1), [x y]] \ z;
a = B(2);
b = B(3);
c = B(1);

zq = a*xq + b*yq + c;

end