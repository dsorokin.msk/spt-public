function convertMeshesToFF(fpathIn, fpathOut, fname, imSize, extrapolate)
% converts set of meshes from consecutive frames to FF ics files for
% evaluations
% 

if (nargin < 5)
    extrapolate = 1;
end

[fpath, fnameNE, ~] = fileparts(fname);
fnames = ls([fpathIn filesep 'cell*.vtu']);
fnames = sortFilenames(fnames);

ffXics = newim(imSize);
ffYics = newim(imSize);
for i=2:size(fnames,1)
    p2 = readVTK_ug([fpathIn filesep fnames(i,:)]);
    p1 = readVTK_ug([fpathIn filesep fnames(i-1,:)]);
%     p1 = [p1(:,2) p1(:,1) p1(:,3)];
%     p2 = [p2(:,2) p2(:,1) p2(:,3)];
    df = p1-p2;
    [xx, yy] = meshgrid(1:imSize(1), 1:imSize(2));
    
    if (extrapolate)
        % add border pixels for FF extrapolation
        % border ff values are set using plane fit to the ff values we have
        % inside the cell
        [p3x, p3y] = meshgrid([1:imSize(1)], [1 imSize(2)]);
        [p4x, p4y] = meshgrid([1 imSize(1)], [1:imSize(2)]);
        border_pnts_x = [p3x(1,:) p3x(2,:) p4x(:,1)' p4x(:,2)'];
        border_pnts_y = [p3y(1,:) p3y(2,:) p4y(:,1)' p4y(:,2)'];
        % we use border pixels with step = 2% of the image max dimension
        step = round(0.02*max(imSize));
        border_pnts_x = border_pnts_x(1:step:end);
        border_pnts_y = border_pnts_y(1:step:end);

        df_x_border_pnts = fitPlaneToScatteredPoints(p2(:,1), p2(:,2), df(:,1), border_pnts_x, border_pnts_y);
        df_y_border_pnts = fitPlaneToScatteredPoints(p2(:,1), p2(:,2), df(:,1), border_pnts_x, border_pnts_y);

    %     figure;
    %     hold on
    %     plot3(p2(:,1),p2(:,2), df(:,1), '.r');
    %     plot3(border_pnts_x, border_pnts_y, zeros(size(border_pnts_y)), '.m');
    %     plot3(border_pnts_x, border_pnts_y, df_x_border_pnts, '.b');
        interp_method = 'linear';
        extrapol_method = 'linear';
    else
        border_pnts_x = [];
        border_pnts_y = [];
        df_x_border_pnts = [];
        df_y_border_pnts = [];
        
        interp_method = 'linear';
        extrapol_method = 'none';
    end

    interpolant_ffx = scatteredInterpolant([p2(:,1); border_pnts_x'], ...
        [p2(:,2); border_pnts_y'], [df(:,1); df_x_border_pnts'], ...
        interp_method, extrapol_method);
    interpolant_ffy = scatteredInterpolant([p2(:,1); border_pnts_x'], ...
        [p2(:,2); border_pnts_y'], [df(:,2); df_y_border_pnts'], ...
        interp_method, extrapol_method);
    
    ffx = interpolant_ffx(xx, yy);
    ffy = interpolant_ffy(xx, yy);
    
    % old deprecated option with extrapolation
%     ffx = griddata([p2(:,1); border_pnts_x'], ...
%         [p2(:,2); border_pnts_y'], [df(:,1); df_x_border_pnts'], xx, yy);
%     ffy = griddata([p2(:,1); border_pnts_x'], ...
%         [p2(:,2); border_pnts_y'], [df(:,2); df_y_border_pnts'], xx, yy);
    % old option with no extrapolation
%     ffx = griddata(p2(:,1),p2(:,2), df(:,1), xx, yy);
%     ffy = griddata(p2(:,1),p2(:,2), df(:,2), xx, yy);
    ffx(isnan(ffx)) = 0;
    ffy(isnan(ffy)) = 0;
    ffXics = slice_in(ffXics, ffx, i-1);
    ffYics = slice_in(ffYics, ffy, i-1);
    dispProgressCounter(i, 2);
%     disp(fnames(i,:))
end
fprintf('\n');

if strfind(fname, 'Bunka1')
    fpathResCell = 'cell1';
elseif strfind(fname, 'Bunka2')
    fpathResCell = 'cell2';
elseif strfind(fname, 'ch00')
    fpathResCell = 'cell3';
end

mkdir([fpathOut]);
if strfind(fnameNE, 'RA')
    copyfile([fname(1:end-4) '_body.tif'], [fpathOut filesep fnameNE '_body.tif']);
    fnameNE = fnameNE(1:end-3);
    copyfile([fpath filesep fnameNE '_rigidM.mat'], [fpathOut filesep fnameNE '_rigidM.mat']);
end

writeim(ffXics, [fpathOut filesep fnameNE '_ffXsh_bcw.ics']);
writeim(ffYics, [fpathOut filesep fnameNE '_ffYsh_bcw.ics']);

end