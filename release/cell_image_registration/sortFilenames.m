function fnamesR = sortFilenames(fnames)

% fnamesR = [];
n = zeros([size(fnames,1) 1]);
for i=1:size(fnames,1)
    p = strsplit(fnames(i,:), '.');
    pp = p{end-1};
    for j=0:length(pp)-1
        if isstrprop(pp(end-j:end), 'digit')
            n(i) = str2num(pp(end-j:end));
        else
            break;
        end
    end
%     pp = sprintf('%s%.2d', pp(1:end-j), n(i));
%     p{end-1} = pp;
%     fnamesR{i} = strjoin(p, '.');
end

[nn, ii] = sort(n);
fnamesR = fnames(ii,:);

end