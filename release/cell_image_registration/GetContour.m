function [P] = GetContour(contourIm, pntsNum, step)
% gets contour from contour image and resamples it uniformly to pntsNum
% points length
% 
% contourIm - binary contour image
% pntsNum - number of points to resample
% step - step between contour points in pixels. if step is not empty,
%        pntsNum is set accordingly
% P - resulting contour curve: P(:,1) - x coordinate, P(:,2) - y coordinate
% !!! returns ONE-BASED contours

if (nargin < 1)
    pntsNum = 60;
end

% [x,y] = ind2sub(size(double(contourIm)),find(double(contourIm) == 1));
% xc = sum(x)/length(x);
% yc = sum(y)/length(y);
%         
% [p a] = cart2pol(double(x-xc),double(y-yc));
% pa = sortrows([p a]);
% p = pa(:,1);
% a = pa(:,2);

% % BE CAREFUL!!! NOT TESTED!!!
% P = GetConsequtiveContourP(contourIm);
P = flipud(im2snake(contourIm))+1;

if nargin > 2
    if isempty(step) || step < 1 || step > size(P,1)
        warning('step is set wrong, pntsNum is set to 100');
    else
        pntsNum = round(size(P,1)/step);
    end
end

x = P(:,1);
y = P(:,2);
xc = sum(x)/length(x);
yc = sum(y)/length(y);
[p a] = cart2pol(double(x-xc),double(y-yc));
% ensure that angle p is increasing (the contour points are sorted
% counterclockwise
if (p(2)-p(1) < 0 && p(1)*p(2) > 0 || p(1)*p(2) < 0 && p(1) < 0)
    p = p(end:-1:1);
    a = a(end:-1:1);
end
% find closest p to 0 to start with it. condition p<0.1 is needed as
% initial p can start with high positive values (like ~3.1)
ind = find(p>0 & p<0.1,1);
p = circshift(p,-ind+1);
a = circshift(a,-ind+1);
[x y] = pol2cart(p,a);
x=x+xc;
y=y+yc;
if (pntsNum > 0)
    P = ResampleContourPoints2D([x y], pntsNum);
else
    P = [x y];
end

% P = [x y];

end