function [fnameOutRes, fnameScene] = createSOFAscript(fnameDummy, fnameOut, poissonRatio, youngMod, compliance, thickness)
% creates SOFA xml script with correct paths
% replaces the word DUMMY in dummy scene by fnameOut
% saves result in fnameScene
% inserts correct values for Young's modulus, Poisson ratio and compliance

if nargin<3
    poissonRatio = 0.45;
end
if nargin<4
    youngMod = 1e5;
end
if nargin<5
    compliance = 0.1;
end
if nargin<6
    thickness = 2e-6;
end

[fnameOutPath, fnameOutNE, ~] = fileparts(fnameOut);
fnameOutRes = sprintf('%s_E%d_P%.2f_C%.1f_T%.0e', fnameOutNE, youngMod, poissonRatio, compliance, thickness);
% fidW = fopen([fnameDummy(1:end-9) fnameOutRes '.scn'], 'w');
fnameScene = [fnameOutPath filesep 'Registration_' fnameOutRes '.scn']; 
fnameOutRes = [fnameOutPath filesep fnameOutRes];
if ~exist(fnameOutPath, 'dir')
    mkdir(fnameOutPath);
end
fidW = fopen(fnameScene, 'w');
fidR = fopen(fnameDummy, 'r');
while(~feof(fidR))
   s = fgetl(fidR);
   s = strrep(s, 'DUMMY_registered', [fnameOutRes '_registered']);
   s = strrep(s, 'DUMMY', fnameOut);
   s = strrep(s, 'youngModulus="1e5"', sprintf('youngModulus="%d"', youngMod));
   s = strrep(s, 'poissonRatio="0.45"', sprintf('poissonRatio="%.2f"', poissonRatio));
   s = strrep(s, 'UncoupledConstraintCorrection compliance="0.1"', ...
         sprintf('UncoupledConstraintCorrection compliance="%.2f"', compliance));
   s = strrep(s, 'thickness="2e-6"', sprintf('thickness="%.0e"', thickness));
   fprintf(fidW,'%s\n',s);
%    disp(s)
end
fclose(fidW);
fclose(fidR);

end