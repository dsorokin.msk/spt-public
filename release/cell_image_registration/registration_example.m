fname = '..\..\data_example\SeqA1.tif';

registration_options.poissonRatio = 0.4;
registration_options.youngMod = 1e4;
registration_options.compliance = 0.5;
registration_options.thickness = 2e-6;
registration_options.createMesh = true;
registration_options.resPathPrefix = '';
registration_options.spotsSuffix = '';

registration_options.pntsN = 100;
registration_options.triHmax = 15;

registration_options.dummy_scene_fname = 'Registration_Dummy.scn';
registration_options.sofa_exe_path = '..\..\sofa-bin\runSofa.exe';
registration_options.tmp_path = 'd:\tmp\';

computeCellMotionDF(fname, 'out', registration_options);

