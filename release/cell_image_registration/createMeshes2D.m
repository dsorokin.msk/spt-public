function createMeshes2D(cellm, fpathOut, pntsN, triHmax)
% 
% creates meshes to use in SOFA for registration based on the binary input
% image
seqLength = imsize(cellm,3);
for i=0:seqLength-1
    b = slice_ex(cellm, i);
    P = GetContour(GetBoundaryIm(b), pntsN);
    if i==0
        [ptri,tri] = getMesh(P, size(double(b)), triHmax, 'inner');
%         figure('Name','Mesh')
%         hold on;
%         plot(ptri(:,1),ptri(:,2),'b.','markersize',1)
%         patch('faces',tri,'vertices',ptri,'facecolor','w','edgecolor', 'm');
        pnts = [ptri zeros([size(ptri,1) 1])];
        elems = [tri];
        fnameOut = sprintf('%s_contour/cell-%02d_mesh.vtk', fpathOut, i);
        writeVTK_ug(fnameOut, pnts, elems);
    end
    pnts = [P zeros([size(P,1) 1])];
    elems = [(1:size(pnts,1)-1)' (2:size(pnts,1))'; size(pnts,1) 1];
    fnameOut = sprintf('%s_contour/cell-%02d.vtk', fpathOut, i);
    writeVTK_ug(fnameOut, pnts, elems);
%     writeVTK_pd(fnameOut, pnts);
end