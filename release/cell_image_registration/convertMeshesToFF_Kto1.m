function convertMeshesToFF_Kto1(fpathIn, fpathOut, fname, imSize)
% converts set of meshes from consecutive frames to FF ics files for
% evaluations
% 

[fpath, fnameNE, ~] = fileparts(fname);
fnames = ls([fpathIn filesep 'cell*.vtu']);
fnames = sortFilenames(fnames);

ffXics = newim(imSize);
ffYics = newim(imSize);
p1 = readVTK_ug([fpathIn filesep fnames(1,:)]);
for i=2:size(fnames,1)
    p2 = readVTK_ug([fpathIn filesep fnames(i,:)]);
%     p1 = readVTK_ug([fpathIn filesep fnames(i-1,:)]);
%     p1 = [p1(:,2) p1(:,1) p1(:,3)];
%     p2 = [p2(:,2) p2(:,1) p2(:,3)];
    df = p1-p2;
    [xx, yy] = meshgrid(1:imSize(1), 1:imSize(2));
    ffx = griddata(p2(:,1),p2(:,2), df(:,1), xx, yy);
    ffy = griddata(p2(:,1),p2(:,2), df(:,2), xx, yy);
    ffx(isnan(ffx)) = 0;
    ffy(isnan(ffy)) = 0;
    ffXics = slice_in(ffXics, ffx, i-1);
    ffYics = slice_in(ffYics, ffy, i-1);
    dispProgressCounter(i, 2);
%     disp(fnames(i,:))
end
fprintf('\n');

if strfind(fname, 'Bunka1')
    fpathResCell = 'cell1';
elseif strfind(fname, 'Bunka2')
    fpathResCell = 'cell2';
elseif strfind(fname, 'ch00')
    fpathResCell = 'cell3';
end

mkdir([fpathOut]);
if strfind(fnameNE, 'RA')
    copyfile([fname(1:end-4) '_body.tif'], [fpathOut filesep fnameNE '_body.tif']);
    fnameNE = fnameNE(1:end-3);
    copyfile([fpath filesep fnameNE '_rigidM.mat'], [fpathOut filesep fnameNE '_rigidM.mat']);
end

writeim(ffXics, [fpathOut filesep fnameNE '_ffXsh_bcw_Kto1.ics']);
writeim(ffYics, [fpathOut filesep fnameNE '_ffYsh_bcw_Kto1.ics']);

end