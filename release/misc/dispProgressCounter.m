function dispProgressCounter(i, startInd)
if nargin<2
    startInd = 1;
end
if i>startInd
    if i>1
        for j=0:log10(i-1)
            fprintf('\b'); % delete previous counter display
        end
    end
    if i==1
        fprintf('\b');
    end
end
fprintf('%d', i);
% pause(.05); % allows time for display to update
% fprintf('\n')
end