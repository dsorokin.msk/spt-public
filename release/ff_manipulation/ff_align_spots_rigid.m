function [spotsRA] = ff_align_spots_rigid(spots, MovlCur2First)

spotsRA{1} = spots{1};
for i=2:length(spots)
    sp = MovlCur2First{i-1}*[spots{i}(:,1:2) ones(size(spots{i},1),1)]';
    if (size(spots{i},2) > 2)
        spotsRA{i} = [sp(1:2,:)' spots{i}(:,3:end)];
    else
        spotsRA{i} = sp(1:2,:)';
    end
end
    
end