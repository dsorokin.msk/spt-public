function [c_reg] = algo_ff_remap_centroids_bcwrd(c, ffX, ffY, interpType)


% check inputs
if (nargin < 4)
    interpType = 0;
end

    X = c(:,1);
    Y = c(:,2);
    
%     ind = X<0|X>size(ffX,2)-1 | Y<0|Y>size(ffX,1)-1;
%     X(ind) = [];
%     Y(ind) = [];
%     c = [X Y];
    
    c_reg = zeros(size(c));
    
    switch interpType
        case 0
            intT = 'nearest';
        case 1
            intT = 'linear';
        case 2
            intT = 'cubic';
        case 3
            intT = 'spline';
    end
    
    ind = Y<=size(ffX,1) & X<=size(ffX,2) & ...
          Y>0 & X>0 & ~isnan(X) & ~isnan(Y);
    
    if (sum(ind) ~= 0)
        fx = get_subpixel(ffX, [X(ind) Y(ind)], intT);
        fy = get_subpixel(ffY, [X(ind) Y(ind)], intT);

        c_reg(ind,1) = X(ind)+fx;
        c_reg(ind,2) = Y(ind)+fy;
        c_reg(~ind,1) = X(~ind);
        c_reg(~ind,2) = Y(~ind);
    else
        c_reg(:,1) = X(:);
        c_reg(:,2) = Y(:);
    end
    
%     fx = get_subpixel(ffX, [X Y], intT);
%     fy = get_subpixel(ffY, [X Y], intT);
%     
%     c_reg(:,1) = X+fx;
%     c_reg(:,2) = Y+fy;
end