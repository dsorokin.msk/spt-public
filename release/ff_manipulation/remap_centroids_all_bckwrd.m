function spotsReg = remap_centroids_all_bckwrd(spots, ffXics, ffYics, interpType, verbose)
% Remaps spots structure using backward deformations fields stacks ffXics 
% and ffYics. Remapping goes N to N-1 consequently
% 
% synopsys:
% spotsReg = remap_centroids_all_bckwrd(spots, ffXics, ffYics, interpType, verbose)
% 
% inputs:
% spots - cell array with coordinates of points spots{i} = [X Y]
% ffXics - x coord of backward deformation field stack image ffXics(t) = ics(t) -> ics(t-1)
% ffYics - y coord of backward deformation field stack image ffYics(t) = ics(t) -> ics(t-1)
% interpType - interpolation type
% verbose - 1/0 = yes/no
% 
% outputs:
% spotsReg - cell array with coordinates of registered points spots{i} = [X Y]

% check inputs
if (nargin < 3)
    interpType = 0;
end

if (nargin < 4)
    verbose = 0;
end

% interpType can be 0 (NN) or 1 (Linear)
if (~ismember(interpType, [0 1]))
    interpType = 0;
    disp('interpType is not 0 or 1. It was set to 0 (Nearest Neighbour interpolation');
end


if (verbose)
    t = tic;
end

% 1:2 is to copy to cReg just X,Y coord, not area information
spotsReg1 = [];
seqLength = length(spots);
if (~isempty(spots))
    if (verbose)
        fprintf(['Remapping... Overall ' num2str(seqLength) ' slices. Progress: ']);
    end
    for j=seqLength:-1:1
        ffX = double(slice_ex(ffXics, j-1));
        ffY = double(slice_ex(ffYics, j-1));
        spotsReg1 = [spotsReg1; spots{j}(:,1:2)];
        spotsReg1 = algo_ff_remap_centroids_bcwrd(spotsReg1, ffX, ...
               ffY, interpType);
        if (any(isnan(spotsReg1)))
            aa = 11;
        end
        if (verbose)
            dispProgressCounter(seqLength-j+1,1)
        end
    end
    if (verbose)
        fprintf('\n');
    end
end

for j=seqLength:-1:1
    spotsReg{j} = spotsReg1(1:size(spots{j},1), 1:2);
    spotsReg1 = spotsReg1(size(spots{j},1)+1:end, :);
    % copy area info back
    if (size(spots{j},2) >= 3)
        spotsReg{j} = [spotsReg{j} spots{j}(:,3:end)];
    end
end

if (verbose)
%     disp('Elasped time for remapping');
    toc(t);
%     disp(['Elapsed time is ' num2str(toc(t)/60) ' minutes.']);
end

end