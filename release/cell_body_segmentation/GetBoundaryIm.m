function cellB = GetBoundaryIm(cellM)
    cellB = extend(cellM,imsize(cellM)+10) - ...
                berosion(extend(cellM,imsize(cellM)+10),1);
    cellB = cut(bskeleton(cellB), imsize(cellM));
end