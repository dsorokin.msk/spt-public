function [res, cSizes] = segmentNucleus(ics, options)
% Segment nucleus time-lapse series
% the parameters are passed throug options struct
% the default values are below
% 
% options.method              = 'triangle'; -       segmentation method
% options.magnCoef            = 1; -                threshold magnification coeff
% options.cellSize            = 75000; -            cellSize parameter for
%                                                   cellSize segmentation method
% options.preprocessFunction  = @preprocessSegmGT;  preprocessing function
%                                                   handle (preprocessing
%                                                   applied at each frame
%                                                   before thresholding)
% options.postprocessFunction = @postprocessSegmGT; postprocessing function
%                                                   handle (postprocessing
%                                                   applied at each frame
%                                                   before thresholding)
% options.contSmoothSigma     = 15;                 sigma for gaussian
%                                                   smoothing at the
%                                                   contour smoothing step
% options.contSmoothThreshold = 0.4;                threshold for gaussian
%                                                   smoothed image at the
%                                                   contour smoothing step
% 
% synopsis:
% [res, cSizes] = segmentNucleus(ics, options)
% 
% outputs:
% res -     segmentation result
% cSizes -  array with cell size in pixels in each frame
% 
% inputs:
% ics -     initial image stack
% options - options structure
% 

if length(size(ics)) < 3
    ics1 = newim([size(ics) 1], datatype(ics));
    ics1(:,:,0) = ics;
    ics = ics1;
end

seqLength = size(ics,3);

res = newim(size(ics), 'bin');
dbgInd = 29;
magnCoef = options.magnCoef;
cellSize = options.cellSize;
% cellSize = c_sizes_smooth;
cSizes = [];

% measure(im1,uint8(im1), 'size')
figure;
fprintf(['Segmenting body. Overall ' num2str(seqLength-1) ' slices. Progress: ']); 
for i=0:seqLength-1
    slice = slice_ex(ics, i);
if (i == dbgInd)
    a = 0;
end
    im1 = slice;
    
    im1 = options.preprocessFunction(im1);
    slicePP = im1;
    
    if strcmpi(options.method, 'watershed')
        [~, tr] = threshold(im1, 'triangle');
        im1 = im1 > tr*magnCoef;
        
        im1 = fillholes(im1);  
        im1 = dip_image(label(im1,inf,10000,0), 'bin');
        im1 = brmedgeobjs(im1);
        
        im1 = closing(im1, options.watershedClosSize);
        g = gradmag(slicePP,options.watershedSigma);
        r = dip_image(watershed(imimposemin(double(g),double(~dilation(im1,options.watershedDilSize) | erosion(im1,options.watershedEroSize))))) == 0;
        im1 = fillholes(r);         
    else
        if strcmpi(options.method, 'CellSize')
            h = diphist(im1);
            h = h(end:-1:1);
            hsum = cumsum(h);
            tr = find(hsum>cellSize, 1, 'first');
            %     tr = find(hsum>cellSize(i+1), 1, 'first');
            tr = 255-tr;
        elseif strcmpi(options.method, 'triangle')
            [~, tr] = threshold(im1, 'triangle');
        elseif strcmpi(options.method, 'otsu')
            [~, tr] = threshold(im1, 'otsu');
        else
            [~, tr] = threshold(im1, 'triangle');
        end
        
        im1 = im1 > tr*magnCoef;
        
    end
    im1 = options.postprocessFunction(im1);
  
% %   for G
%     im1 = gaussf(uint8(im1), 5)>0.5;
% %   for R
    if ~(options.contSmoothSigma == 0 && options.contSmoothThreshold == 0) 
        imSz = imsize(im1);
        im1 = extend(im1, imSz+40);
        if options.contSmoothKeepEdges == 1
            g = gradmag(slice);
            strong_g = g > max(g)*options.contSmoothEdgeStrength;
            sm = stretch(gaussf(im1,options.contSmoothSigma));
            sm1 = stretch(gaussf(strong_g,10));
            strong_g_mask = sm < sm1;
            im1 = gaussf(opening((im1 & strong_g_mask) | (sm > 100 & ~strong_g_mask)))>0.4;
        else
            im1 = gaussf(uint8(im1), options.contSmoothSigma)>options.contSmoothThreshold;
        end
        im1 = cut(im1, imSz);
    end
    
    
     m = measure(im1, 255*im1, 'size');
     if length(m) > 0
        cSizes = [cSizes m(1).size];
     end
      
    
    im1 = fillholes(im1);

% % Delete apendixes
%     trash = im1 - opening(im1, 80);
%     
% %     measure(trash, 255*trash, 'size')
%     trash = dip_image(label(trash,inf,100,0), 'bin');
%     im1 = im1-trash;
    
    res = slice_in(res, im1>0, i);

    ovl = uint8(overlay(slicePP, im1-berosion(im1), [255 0 0]));
    imshow(ovl)
    text(20,20,['Frame: ' int2str(i)], 'Color', 'White');
    drawnow
    
    dispProgressCounter(i,0)
end
fprintf('\n');

if seqLength == 1
    res = slice_ex(res,0);
end

end