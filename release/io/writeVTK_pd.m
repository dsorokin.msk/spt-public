function writeVTK_pd(fname, pnts)
% writes polydata VTK file
% Limited functionality! Do not fully support poldata format.
% Writes only set of points as vertices
% 

np = size(pnts,1);
fprintf('Saving to %s\n', fname);
fid = fopen(fname,'w');
fprintf(fid,'# vtk DataFile Version 2.0\n');
fprintf(fid,'vtk output\n');
fprintf(fid,'ASCII\n');
fprintf(fid,'DATASET POLYDATA\n');
fprintf(fid,'POINTS %d float\n', np);
% for i=1:np
fprintf(fid, '%g %g %g\n', pnts');
% end

fprintf(fid,'\n');
fprintf(fid,'LINES 1 %d\n', np+1);
fprintf(fid,'%d ', np);
for i=1:np
    fprintf(fid, '%d ', i-1);
end
fprintf(fid,'\n');
fclose(fid);    

end