function [tracks] = ICYtracksRead(fname)
% Reads the tracks from ICY xml file 
% 
% synopsys:
% [spots] = ICYspotsRead(fname)
% 
% outputs:
% tracks -    array with tracks: tr_id x y z t 0
% 
% inputs:
% fname -       filename
% 

tracks = [];
trId = 1;
a = xmlread(fname);

trGroups = a.getElementsByTagName('trackgroup');
trs = a.getElementsByTagName('track');

fprintf(['Reading tracks. Overall ' num2str(trs.getLength) '. Progress: ']);
for g=0:trGroups.getLength-1
    trGroup = trGroups.item(g);
    trs = trGroup.getElementsByTagName('track');
    for t=0:trs.getLength-1
        tr = trs.item(t);
        trackPoints = tr.getElementsByTagName('detection');
        for i=0:trackPoints.getLength-1
            b(1) = str2double(trackPoints.item(i).getAttribute('x'));
            b(2) = str2double(trackPoints.item(i).getAttribute('y'));
            b(3) = str2double(trackPoints.item(i).getAttribute('z'));
            b(4) = str2double(trackPoints.item(i).getAttribute('t'))+1;
            tracks = [tracks; trId b 0];
        end
        trId = trId + 1;
        dispProgressCounter(t+1,1)
    end
end
fprintf('\n');


end
