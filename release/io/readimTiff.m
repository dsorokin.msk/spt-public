function [im] = readimTiff(fname)

if ~strcmp(fname(end-3:end), '.tif') && ~strcmp(fname(end-4:end), '.tiff')
    error('Wrong extention. Not a TIFF image.');
end

iminfo = imfinfo(fname);
num_images = numel(iminfo);
a = imread(fname, 1, 'Info', iminfo);
if (length(size(a)) == 2)
    if (islogical(a))
        a = zeros(iminfo(1).Height, iminfo(1).Width, num_images) > 0;
    else
        a = zeros(iminfo(1).Height, iminfo(1).Width, num_images, class(a));
    end
    % fprintf(['Overall ' num2str(num_images) ' slices. Progress: ']);
    for k = 1:num_images
        a(:,:,k) = imread(fname, k, 'Info', iminfo);
    %     dispProgressCounter(k)
    end
    % fprintf('\n');
    im = dip_image(a);
elseif (length(size(a)) == 3)
    for c=1:3
        arr{c} = zeros(iminfo(1).Height, iminfo(1).Width, num_images, class(a));
    end
    % fprintf(['Overall ' num2str(num_images) ' slices. Progress: ']);
    for k = 1:num_images
        a = imread(fname, k, 'Info', iminfo);
        for c=1:3
            arr{c}(:,:,k) = a(:,1:end,c);
        end
    %     dispProgressCounter(k)
    end
    % fprintf('\n');
    if (num_images > 1)
        im = newcolorim([iminfo(1).Width, iminfo(1).Height, num_images]);
    else
        im = newcolorim([iminfo(1).Width, iminfo(1).Height]);
    end
    for c=1:3
        im{c} = dip_image(arr{c});
    end
else
    error('Wrong image type. More than 3 color channels.');
end

end