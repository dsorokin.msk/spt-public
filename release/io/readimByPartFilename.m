function [im, fname, finfo] = readimByPartFilename(fpath, fnamePart)

fnames = ls([fpath filesep '*' fnamePart '*']);

if (size(fnames,1) >1)
    warning('more than 1 match. the first file is opened')
end
if isempty(fnames)
    fname = [];
    im = [];
    finfo = [];
    return;
end
fname = strtrim(fnames(1,:));
fname = [fpath filesep fname];
if (strcmpi(fname(end-3:end), '.tif'))
    im = readimTiff(fname);
else
    [im, finfo] = readim(fname);
end



end