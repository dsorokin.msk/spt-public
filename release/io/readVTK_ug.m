function [pnts, cells, pntsData] = readVTK_ug(fname)
% read VTK unstructured grid
% 

fid = fopen(fname,'r');
if( fid==-1 )
    error('Can''t open the file.');
    return;
end

str = fgets(fid);   % -1 if eof
if ~strcmp(str(3:5), 'vtk')
    error('The file is not a valid VTK one.');    
end

%%% read header %%%
strP = [];
while (isempty(strP) || ~strcmpi(strP{1}, 'DATASET'))
    str = fgets(fid);
    strP = strsplit(str, ' ');
end
vtkType = strtrim(strP{2});
if ~strcmpi(vtkType, 'UNSTRUCTURED_GRID')
    error('Dataset type is not UNSTRUCTURED_GRID.');    
end

% read type
% strP = [];
% while (isempty(strP) || ~strcmpi(strP{2}, 'UNSTRUCTURED_GRID'))
%     str = fgets(fid);
%     if str2num(str) == -1
%         error('Dataset type is not UNSTRUCTURED_GRID.');    
%     end
%     strP = strsplit(str, ' ');
% end

str = fgets(fid);
npnts = sscanf(str,'%*s %d %*s', 1);
dType = strsplit(str, ' ');
dType = dType{end};

% switch(dType)
%     case 'char', type='int8';
%     case 'uchar', type='uint8';
%     case 'short', type='int16';
%     case 'ushort', type='uint16';
%     case 'int', type='int32';
%     case 'uint', type='uint32';
%     case 'float', type='single';
%     case 'double', type='double';
%     otherwise, type='double';
% end

% read vertices
[A, cnt] = fscanf(fid,'%f %f %f', 3*npnts);
if cnt~=3*npnts
    warning('Problem in reading vertices.');
end
A = reshape(A, 3, cnt/3);
pnts = A';

% read CELLS
strP = [];
while (isempty(strP) || ~strcmpi(strP{1}, 'CELLS'))
    str = fgets(fid);
    if str2num(str) == -1
        error('CELLS section is missing.');    
    end
    strP = strsplit(str, ' ');
end

cells = struct;
ncells = str2double(strP{2});
for i=1:ncells
    cells(i).idx = [];
    cells(i).type = [];
    str = fgets(fid);
    strP = textscan(str,'%d');
    cells(i).idx = strP{1}(2:end)';
end

% read CELL_TYPES
strP = [];
while (isempty(strP) || ~strcmpi(strP{1}, 'CELL_TYPES'))
    str = fgets(fid);
    if str2double(str) == -1
        error('CELL_TYPES section is missing.');    
    end
    strP = strsplit(str, ' ');
end

ncells = str2double(strP{2});
if ncells ~= str2double(strP{2})
    error('Number of CELL_TYPES is different from number of CELLS.');    
end

[ct, cnt] = fscanf(fid,'%f', ncells);

for i=1:ncells
%     str = fgets(fid);
    cells(i).type = ct(i);
end

% read CELL_DATA
strP = [];
while (isempty(strP) || ~strcmpi(strP{1}, 'CELL_DATA'))
    str = fgets(fid);
    if feof(fid)
        fclose(fid);
        return;
    end
    if str2double(str) == -1
        warning('CELL_DATA section is missing.');    
    end
    strP = strsplit(str, ' ');
end

% ncells = str2double(strP{2});
if ncells ~= str2double(strP{2})
    error('Number of CELL_DATA is different from number of CELLS.');    
end

str = fgets(fid);
strP = strsplit(str, ' ');
cell_data_type1 = strtrim(strP{1});
cell_data_name = strtrim(strP{2});
cell_data_type = strtrim(strP{3});
cell_data_type_sz = strtrim(strP{4});

str = fgets(fid);% skip LOOKUP_TABLE default

[cdata, cnt] = fscanf(fid,'%f', ncells*str2double(cell_data_type_sz));

for i=1:ncells*str2double(cell_data_type_sz)
%     str = fgets(fid);
    cells(i).celldata = cdata(i);
end

% read POINT_DATA
strP = [];
while (isempty(strP) || ~strcmpi(strP{1}, 'POINT_DATA'))
    str = fgets(fid);
    if feof(fid)
        fclose(fid);
        return;
    end
    if str2double(str) == -1
        warning('POINT_DATA section is missing.');    
    end
    strP = strsplit(str, ' ');
end

% ncells = str2double(strP{2});
if npnts ~= str2double(strP{2})
    error('Number of POINT_DATA is different from number of POINTS.');    
end

str = fgets(fid);
strP = strsplit(str, ' ');
point_data_type1 = strtrim(strP{1});% scalar/vector
point_data_name = strtrim(strP{2});
point_data_type = strtrim(strP{3});% float/double/int/etc.
if length(strP) == 4
    point_data_type_sz = strtrim(strP{4});% if vector than how many elements
else
    point_data_type_sz = '1';
end

str = fgets(fid);% skip LOOKUP_TABLE default

[pntsData, cnt] = fscanf(fid,'%f', npnts*str2double(point_data_type_sz));

% for i=1:ncells*str2double(point_data_type_sz)
% %     str = fgets(fid);
%     cells(i).pointdata = pdata(i);
% end

fclose(fid);

end