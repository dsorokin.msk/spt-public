function [] = writeimTiff(im, fname)

if ~strcmp(fname(end-3:end), '.tif') && ~strcmp(fname(end-4:end), '.tiff')
    warning('wrong extention. appended ".tif"');
    fname = [fname '.tif~'];
else
    fname = [fname '~'];
end

if (imsize(im,3) < 2)
    writeim(im, fname, 'TIFF', 'no')
    return;
end

n = prod(imarsize(im));
if (n == 1)
    dtype = datatype(im);
elseif (n==3)
    [dtype,dtype2,dtype3] = datatype(im);
    if (~strcmp(dtype, dtype2) || ~strcmp(dtype, dtype3))
%         error('Image type is not supported. DATATYPEs of imarray elements are not consistent.')
        warning('Type of data for each channel is different! Everything is converted to double!.')
        im = dip_image(im, 'double');
        dtype = datatype(im{1});
    end
else
    error('Image type is not supported. Too many elements in imarray.')
end

if (strcmp(dtype, 'bin'))
    imwrite(im2mat(slice_ex(im,0), dtype)>0, fname, 'tif', 'Compression', 'none');
    for i=1:imsize(im,3)-1
        imwrite(im2mat(slice_ex(im,i), dtype)>0, fname, 'tif', 'writemode', 'append', ...
                'Compression', 'none');
    end
else
    if (strcmp(dtype, 'sfloat') || strcmp(dtype, 'double'))
        dtype = 'double';
        imwrite(im2mat(slice_ex(im,0), dtype), fname, 'tif', 'Compression', 'none');
        for i=1:imsize(im,3)-1
            imwrite(im2mat(slice_ex(im,i), dtype), fname, 'tif', 'writemode', 'append', ...
                    'Compression', 'none');
        end
    else
        imwrite(im2mat(slice_ex(im,0), dtype), fname, 'tif', 'Compression', 'none');
        for i=1:imsize(im,3)-1
            imwrite(im2mat(slice_ex(im,i), dtype), fname, 'tif', 'writemode', 'append', ...
                    'Compression', 'none');
        end
    end
end

movefile(fname, fname(1:end-1))

end