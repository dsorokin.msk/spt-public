function writeSpots(spots, fname, spotsSuffix)
% write spots to the .mat file and create and write overlays
% 
% synopsys:
% writeSpots(spots, fname, spotsSuffix, smaskSuffix, ovlSuffix)
% 
% inputs:
% spots -       cell array of coordinates of centroids of found spots
% fname -       filename of the initial stack
% spotsSuffix - suffix for spots structure
% 

if  nargin < 3
    spotsSuffix = 'spots';
end
    
fnameSpots = [fname(1:end-4) '_' spotsSuffix '.mat'];
save(fnameSpots, 'spots');

end