function ICYspotsWrite(spots, fname)
% Write the spots coordinates to ICY Spot Detector Import/Export Format 
% 
% synopsys:
% [spots] = ICYspotsRead(fname)
% 
% inputs:
% spots -    structure with spots
% fname -       filename
% 

a = [];
for i=1:length(spots)
    b = [repmat(i-1, size(spots{i},1),1) spots{i}(:,1:2) zeros(size(spots{i},1), 4)];
	a = [a;b];
end
% save(fname, 'a', '-ascii', '-double');

fout = fopen(fname,'w');
for i=1:size(a,1)
    fprintf(fout,'%d %.12f %.12f %.12f %.12f %.12f %.12f\n', a(i,:));
end
fclose(fout);


end
