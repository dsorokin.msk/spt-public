function [spots] = ICYspotsRead(fname)
% Reads the spots coordinates from ICY Spot Detector Import/Export Format 
% 
% synopsys:
% [spots] = ICYspotsRead(fname)
% 
% outputs:
% spots -    structure with spots
% 
% inputs:
% fname -       filename
% 

a = load(fname);

maxTime = max(a(:,1));
spots = [];

for i=0:maxTime
    b = a(a(:,1)==i,:);
    spots{i+1} = [b(:,2:3) zeros(size(b,1),1)];
end

end
