function writeVTK_ug(fname, pnts, elems, cellData, pointData)
% writes unstructured grid VTK file
% Limited functionality! Do not fully support unstructured grid format.
% 

np = size(pnts,1);
fprintf('Saving to %s\n', fname);
fid = fopen(fname,'w');
fprintf(fid,'# vtk DataFile Version 2.0\n');
fprintf(fid,'vtk output\n');
fprintf(fid,'ASCII\n');
fprintf(fid,'DATASET UNSTRUCTURED_GRID\n');
fprintf(fid,'POINTS %d float\n', np);
% for i=1:np
fprintf(fid, '%g %g %g\n', pnts');
% end

% elems(:,end) = [];
nel = size(elems,1);
fprintf(fid,'\n');
fprintf(fid,'CELLS %d %d\n', nel, nel*(size(elems,2) + 1));
% fprintf(fid,'%d ', np);
if size(elems,2) == 2
    format_str = '%d %d %d\n';
elseif size(elems,2) == 3
    format_str = '%d %d %d %d\n';
elseif size(elems,2) == 4
    format_str = '%d %d %d %d %d\n';
end
fprintf(fid, format_str, [ones([nel 1])*size(elems,2) elems-1]');

if size(elems,2) == 2
    cType = 3;
elseif size(elems,2) == 3
    cType = 5;
elseif size(elems,2) == 4
    cType = 10;
end
fprintf(fid,'\n');
fprintf(fid,'CELL_TYPES %d\n', nel);
fprintf(fid, '%d\n', [ones([nel 1])*cType]');

% write cellData
if nargin>3 && ~isempty(cellData)
    if isstruct(cellData)
        cd = cellData.data;
        cdName = cellData.name;
        cdType = class(cellData);
    else
        cd = cellData;
        cdName = 'cellDataName';
        cdType = class(cellData);
    end
    fprintf(fid,'\n');
    fprintf(fid,'CELL_DATA %d\n', nel);
    fprintf(fid,'SCALARS %s %s 1\n', cdName, cdType);
    fprintf(fid, '%d\n', cd');
end

% write pointData
if nargin>4 && ~isempty(pointData)
    if isstruct(pointData)
        pd = pointData.data;
        pdName = pointData.name;
        pdType = class(pointData);
    else
        pd = pointData;
        pdName = 'pointDataName';
        pdType = class(pointData);
    end
    fprintf(fid,'\n');
    fprintf(fid,'POINT_DATA %d\n', np);
    fprintf(fid,'SCALARS %s %s 1\n', pdName, pdType);
    fprintf(fid, '%d\n', pd');
end

fclose(fid);    

end