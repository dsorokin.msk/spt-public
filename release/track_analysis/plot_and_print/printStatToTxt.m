function printStatToTxt(stat, statDir, modelParams, fname)
% prints tracks statistics to txt file

if isempty(statDir)
    create_stat_dir_figs = 0;
    for i=1:length(stat)
        statDir(i).dir_ang = [];
        statDir(i).nb_ang_diff = [];
        statDir(i).minDind = [];
        statDir(i).nuc_ang_diff = [];
    end
end

headingStrFrmt = ['TrackID\tNumber Of Frames\tX start (px)\tY start (px)\t'...
                  'Ellipse Area (micron^2)\tTotal dist. (micron)\tAVG Dist-per-frm (micron)\t STD Dist-per-frm (micron)\t'...
                  'AVG Veloc (micron/s)\tAVG area (micron^2)\tSTD area (micron^2)\t'...
                  'Diff. coeff. (nm^2/s)\tv or L\tMotion type\t'...
                  'Error norm\tMotion dir. ang.\tTowards NB dev. ang.\tClosest NB\tTowards center dev. ang.' char([13, 10])];
dataStrFrmt = ['%d\t%5d\t%4.1f\t%4.1f\t' '%4.5f\t%4.3f\t%4.3f\t%4.3f\t' ...
               '%4.5f\t%4.3f\t%4.3f\t' '%4.3f\t%4.3f\t%c\t' ...
               '%4.5f\t%4.1f\t%4.1f\t%d\t%4.1f' char([13, 10])];

statArr = [[1:length(stat)]' [stat.num_frames]' [stat.start_x]' [stat.start_y]' ...
           [stat.areaEll]' [stat.total_dist]' [stat.avg_dist]' [stat.std_dist]' ...
           [stat.avg_veloc]' [stat.avg_area]' [stat.std_area]' ...
           [modelParams.D]' [modelParams.secPar]' double([modelParams.motionTypeC]') ...
           [modelParams.errorNorm]' [statDir.dir_ang]' [statDir.nb_ang_diff]' ...
           [statDir.minDind]' [statDir.nuc_ang_diff]'];

if (~isempty(fname))
    fid = fopen(fname, 'w');
else
    fid = 1;
end
fprintf(fid, 'Total number of \tNormal\tFlow\tConf\n');
fprintf(fid, '\t%d\t%d\t%d\n', sum([modelParams.motionTypeC]=='N'), sum([modelParams.motionTypeC]=='F'), sum([modelParams.motionTypeC]=='C'));
fprintf(fid, 'Tracks statistics:\n');
fprintf(fid, headingStrFrmt);
% sstat = sortrows(stat, 3); % sort by the track length
fprintf(fid, dataStrFrmt, statArr');
fprintf(fid, '\n');
if (~isempty(fname))
    fclose(fid);
end

%% save statDir pics if directional statistics was computed

if create_stat_dir_figs
    createAndSaveStatDirFigures(fname, modelParams, statDir)
end

end