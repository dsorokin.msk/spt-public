function plotSimpleMSDfitResult(msd, minInd, estParams)

figure
hold on
plot(1:length(msd(:)), msd(:), '.b');
switch minInd
    case 1
        col = '-g';
    case 2
        col = '-c';
    case 3
        col = '-k';
end
res = estParams{minInd}.res;
plot(1:length(res(:)), res(:), col);
% if nargin>2
%     for i=1:3
%         disp(estParams{i})
%     end
% end


end