% plot_all_tracks(create_tracks(), img)
% img .... the tracks are overlayed over the image img (default [] meaning
% no image)
% plot_all_tracks_ellipce(t, img, show_ids, central_ids, px_size, ellongTr, areaTr, axisSz, dispStat)
function plot_all_tracks_ellipce(tl, img, show_ids, central_ids, px_size, ellongTr, areaTr, axisSz, dispStat, ellColor, motionType)

if (nargin < 11)
    motionType = [];
    ellColor = 'black';
end

if (nargin < 10 || isempty(ellColor))
    ellColor = 'black';
end

if (nargin < 9)
    dispStat = 0;
end

if (nargin < 8)
    axisSz = [];
end

if nargin < 7
    areaTr = 1000000;
end

if nargin < 6
    ellongTr = 100;
end

if nargin < 5
    px_size = 1;
end

if nargin < 4 || show_ids == 0
    central_ids = zeros(1,length(tl));
end

if nargin < 3
    show_ids = 0;
end

if nargin < 2
    img = [];
end

if (~isempty(motionType))
    colorspec(1) = 'g';
    colorspec(2) = 'c';
    colorspec(3) = 'k';
end

%figure
cmap = colormap;
axis equal
% hold off

% ellongTr = 5;

avgArea = 0;
avgAreaW = 0;

% if (isempty(axisSz))
%     axisSz = [0 0 0 0];
% end

if isa(tl, 'containers.Map')
    tl = tl.values;
end

if isempty(img)

    maxTime = 0;
    for i = 1:length(tl)
        for j = 1:length(tl{i}.tracks)
            maxTime = max([maxTime tl{i}.tracks{j}(:,[5])']);
        end
    end
    
    for i = 1:length(tl)
%         if (size(tl{i}.convhull, 1) > 2)
            [A, c] = MinVolEllipse(tl{i}.convhull', 0.01);
%         else
%             ch = tl{i}.convhull;
%             % add 2 points on the distance k to the midpoint between the
%             % ch(1,: and ch(2,:)
%             k = sqrt(sum((ch(1,:)-ch(2,:)).^2))/2;
%             M = mean(ch, 1);
%             v = ch(1,:)-ch(2,:);
%             D1(1) = M(1)-sqrt(k*k/(1+v(1)*v(1)/v(2)/v(2)));
%             D1(2) = M(2)-v(1)*D1(1)/v(2);
%             D2(1) = M(1)+sqrt(k*k/(1+v(1)*v(1)/v(2)/v(2)));
%             D2(2) = M(2)-v(1)*D2(1)/v(2);
%             ch = [ch; D1; D2];
%             [A, c] = MinVolEllipse(ch', 0.01);
%         end
        [U D V] = svd(A);
        a = 1/sqrt(D(1,1));
        b = 1/sqrt(D(2,2));
        
        if (max([a b]) / min([a b]) > ellongTr || pi*a*b > areaTr)
            fprintf('%d - %.2f - %.2f\n', i, max([a b]) / min([a b]), pi*a*b);
        end
        
        if (max([a b]) / min([a b]) < ellongTr && pi*a*b < areaTr)
            p = [];
            for j = 1:length(tl{i}.tracks)
                p = [p; tl{i}.tracks{j}(:,[2 3])];
            end
            p = p * px_size;
    %        plot(p(:,1), -p(:,2), 'Color', cmap(mod(i,size(cmap, 1)) + 1, :));
            plot(p(:,1), p(:,2), 'Color', 'b', 'LineWidth', 1);
            hold on

            if (~isempty(motionType))
                ellColor = colorspec(motionType(i)+1);
            end
                
            Ellipse_plot(A, c, 20, px_size, ellColor)
            if central_ids(i) 
                plot(p(1,1), p(1,2), 'or','LineWidth',2);
            else 
                plot(p(1,1), p(1,2), '.r','LineWidth',2);            
            end
            if show_ids == 1
                text(p(1,1)+5* px_size, p(1,2)+5* px_size, sprintf('%d(%d)', central_ids(i),size(p,1))); 
            elseif  show_ids == 2
                text(p(1,1), p(1,2), sprintf('%d(%d)', central_ids(i),size(p,1)), 'BackGroundColor', 'White'); 
            end
            if (dispStat)
                ellArea = pi*a*b*px_size*px_size;
                trLength = size(p,1);
                avgArea = avgArea + ellArea;
                avgAreaW = avgAreaW + ellArea*trLength/maxTime;
                fprintf('Statistics for particle %d: area %.3f length %d\n',...
                                i,ellArea, trLength);
            end
        end
    end
    if (dispStat)
        fprintf('Average area: %.3f; Average weightened area %.3f\n',...
                                    avgArea/length(tl), avgAreaW/length(tl));
        fprintf('Sequence length : %d\n', maxTime);
    end
    grid off;
    if (~isempty(axisSz))
        axis(axisSz)
        axis normal
        xLength = axisSz(2)-axisSz(1);
        xTick = linspace(0, xLength, 6);
        set(gca,'XTick', axisSz(1)+xTick')
        set(gca,'XTickLabel',xTick')
        yLength = axisSz(4)-axisSz(3);
        yTick = linspace(0, yLength, 6);
        set(gca,'YTick', axisSz(3)+yTick')
        set(gca,'YTickLabel',yTick')
    else
        axis equal
    end
    set(gca,'ydir','reverse')
    yLimits = get(gca,'YLim');  %# Get the y axis limits
    yTicks = yLimits(2)-get(gca,'YTick');  %# Get the y axis tick values and
                                           %#   subtract them from the upper limit
    set(gca,'YTickLabel',num2str(yTicks.'));
%     set(gca, 'LineWidth', 2)
%     set(gca, 'FontSize', 20)
    xlabel('\mum')
    ylabel('\mum')
%     xlabel('\mum', 'FontSize', 20)
%     ylabel('\mum', 'FontSize', 20)
else
    x = (0:size(img, 2)-1) * px_size;
    y = (0:size(img, 1)-1) * px_size;
    
%     [X, Y] = meshgrid(x, y);
%     surf(X, Y, double(img),'LineStyle', 'none')
    imagesc(x,y,img);
%     set(gca,'ydir','normal')
    xLength = floor((size(img, 2)-1) * px_size/5)*5;
    xTick = linspace(0, xLength, 6);
    set(gca,'XTick', xTick')
    set(gca,'XTickLabel',xTick')
    yLength = floor((size(img, 1)-1) * px_size/5)*5;
    yTick = linspace((size(img, 1)-1) * px_size-yLength, (size(img, 1)-1) * px_size, 6);
    yTickLabel = linspace(yLength, 0, 6);
    set(gca,'YTick', yTick')
    set(gca,'YTickLabel',yTickLabel')
%     imshow(img)
%     px_size_old = px_size;
%     px_size = 1;
    hold on
    if (size(img,3) <3)
        colormap grey
    end
%     view(0, -90)
    
    zlevel = -100;
    for i = 1:length(tl)
        [A, c] = MinVolEllipse(tl{i}.convhull', 0.01);
        N = 20; % Default value for grid
        % "singular value decomposition" to extract the orientation and the
        % axes of the ellipsoid
        [U D V] = svd(A);

        % get the major and minor axes
        %------------------------------------
        a = 1/sqrt(D(1,1));
        b = 1/sqrt(D(2,2));
        
        if (max([a b]) / min([a b]) < ellongTr && pi*a*b < areaTr)

            theta = [0:1/N:2*pi+1/N];

            % Parametric equation of the ellipse
            %----------------------------------------
            state(1,:) = a*cos(theta);
            state(2,:) = b*sin(theta);

            % Coordinate transform
            %----------------------------------------
            X = V * state;
            X(1,:) = X(1,:) + c(1);
            X(2,:) = X(2,:) + c(2);

%             plot(X(1,:) * px_size, X(2,:) * px_size, 'Color', 'blue');
            plot(X(1,:) * px_size, X(2,:) * px_size, 'Color', 'Yellow', 'LineWidth', 2);
            
            plot(p(:,1), p(:,2), 'Color', 'b', 'LineWidth', 1);
            hold on

            if (~isempty(motionType))
                ellColor = colorspec(motionType(i)+1);
            end
                
            Ellipse_plot(A, c, 20, px_size, ellColor)
            if central_ids(i) 
                plot(p(1,1), p(1,2), 'or','LineWidth',2);
            else 
                plot(p(1,1), p(1,2), '.r','LineWidth',2);            
            end
            if show_ids == 1
                text(p(1,1)+5* px_size, p(1,2)+5* px_size, sprintf('%d(%d)', central_ids(i),size(p,1))); 
            elseif  show_ids == 2
                text(p(1,1), p(1,2), sprintf('%d(%d)', central_ids(i),size(p,1)), 'BackGroundColor', 'White'); 
            end
            
%             plot3(X(1,:) * px_size, X(2,:) * px_size, zlevel * ones(size(X(2,:))), 'y');
%     %        plot3(c(1),c(2),zlevel,'r*');

            p = [];
            for j = 1:length(tl{i}.tracks)
                p = [p; tl{i}.tracks{j}(:,[2 3])];
            end
            p = p * px_size;
            

            plot(p(:,1), p(:,2), 'c');
%             plot(p(1,1), p(1,2), '.r');
            plot(p(1,1), p(1,2), '.r', 'MarkerSize', 10);
            
%             plot3(p(1,1), p(1,2), 2*zlevel, '.r');
%     %        plot3(p(:,1), p(:,2), zlevel * ones(size(p(:,2))), 'Color', cmap(mod(i,size(cmap, 1)) + 1, :));
%             plot3(p(:,1), p(:,2), zlevel * ones(size(p(:,2))), 'c');

        end
        
        if (~isempty(axisSz))
            axis(axisSz)
        else
            axis tight             
        end
        xlabel('\mum')
        ylabel('\mum')
        axis off
    end
end

