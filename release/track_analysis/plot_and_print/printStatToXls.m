function printStatToXls(stat, statDir, modelParams, nbInd, fname, sheet, picsPath)
% prints tracks statistics to excel file

if isempty(statDir)
    create_stat_dir_figs = 0;
    for i=1:length(stat)
        statDir(i).dir_ang = [];
        statDir(i).nb_ang_diff = [];
        statDir(i).minDind = [];
        statDir(i).nuc_ang_diff = [];
    end
end

headingStrFrmt = {'TrackID','Number Of Frames','X start (px)','Y start (px)',...
                  'Ellipse Area (micron^2)','Total dist. (micron)','AVG Dist-per-frm (micron)',' STD Dist-per-frm (micron)',...
                  'AVG Veloc (micron/s)','AVG area (micron^2)','STD area (micron^2)',...
                  'Diff. coeff. (micron^2/s * 10^-5)','v or L * 10^-3','Motion type',...
                  'Error norm','Motion dir. ang.','Towards NB dev. ang.','Closest NB',...
                  'Towards center dev. ang.', 'Local radius'};

pnbInd = setdiff(1:length(stat), nbInd);
stat = stat(pnbInd);
statDir = statDir(pnbInd);
modelParams = modelParams(pnbInd);

statArr = [[pnbInd]' [stat.num_frames]' [stat.start_x]' [stat.start_y]' ...
           [stat.areaEll]' [stat.total_dist]' [stat.avg_dist]' [stat.std_dist]' ...
           [stat.avg_veloc]' [stat.avg_area]' [stat.std_area]' ...
           [modelParams.D]'*100000 [modelParams.secPar]'*1000 double([modelParams.motionTypeC]') ...
           [modelParams.errorNorm]' [statDir.dir_ang]' [statDir.nb_ang_diff]' ...
           [statDir.minDind]' [statDir.nuc_ang_diff]' [stat.lr]'];

xlswrite(fname, {'Total number of ', 'Normal', 'Flow', 'Conf'}, sheet, 'A1');
xlswrite(fname, {'',sum([modelParams.motionTypeC]=='N'), sum([modelParams.motionTypeC]=='F'), sum([modelParams.motionTypeC]=='C')}, sheet, 'A2');
xlswrite(fname, {'Tracks statistics:'}, sheet, 'A3');

xlswrite(fname, headingStrFrmt, sheet, 'A4');
xlswrite(fname, statArr, sheet, 'A5');
xlswrite(fname, [modelParams.motionTypeC]', sheet, 'N5');

if create_stat_dir_figs
    main_pic_cell = 'U5';
else
    main_pic_cell = 'P4';
end
[~,f] = readimByPartFilename([picsPath filesep], '_overlay_1frame_w_ids');
pic2xls(f, fname, sheet, [main_pic_cell], 0.18)

if create_stat_dir_figs
    ll = length(stat);
    [~,f] = readimByPartFilename([picsPath filesep], '_mot-dir-ang');
    pic2xls(f, fname, sheet, ['A' num2str(ll+5)], 0.35);
    [~,f] = readimByPartFilename([picsPath filesep], '_NB-dev-ang');
    pic2xls(f, fname, sheet, ['H' num2str(ll+5)], 0.35);
    [~,f] = readimByPartFilename([picsPath filesep], '_center-dev-ang');
    pic2xls(f, fname, sheet, ['Q' num2str(ll+5)], 0.35);
end

end