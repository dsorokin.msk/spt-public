% plot_all_tracks(create_tracks(), img)
% img .... the tracks are overlayed over the image img (default [] meaning
% no image)
% plot_all_tracks_ellipce(t, img, show_ids, central_ids, px_size, ellongTr, areaTr, axisSz, dispStat)
function f = plotTracksEllipcesOverImage(tl, im, show_ids, px_size, ellColor, motionType, zoomFactor, axisSize, showStart, showEnd, nbInd)

if nargin < 11
    nbInd = [];
end

if nargin < 10
    showEnd = 0;
end

if nargin < 9
    showStart = 0;
end

if nargin < 8
    axisSize = [];
end

if nargin < 7
    zoomFactor = 1;
end

if nargin < 6
    motionType = [];
end

if nargin < 5
    ellColor = [];
end
% if isempty(ellColor)
%     ellColor = 'y';
% end

% im = double(resample(im, zoomFactor));
% px_size = px_size/zoomFactor;
% for i=1:length(tl)
%     for j=1:length(tl{i}.tracks)
%         tl{i}.tracks{j}(:,2:3) = tl{i}.tracks{j}(:,2:3)*zoomFactor;
%     end
% end
% tl = recomputeConvhullEllipses(tl);

tlRaw = tl;
if isa(tlRaw, 'containers.Map')
    tl = tlRaw.values;
end

if ~isempty(im)
    im = double(im);
end

colorspec(1) = 'g';
colorspec(2) = 'c';
colorspec(3) = 'k';

x = (0:size(im, 2)-1) * px_size;
y = (0:size(im, 1)-1) * px_size;

%     [X, Y] = meshgrid(x, y);
%     surf(X, Y, double(img),'LineStyle', 'none')
f = figure;
if ~isempty(im)
    imagesc(x,y,im, [-100 350]);
        % imshow(uint8(im/2)+120);
    %     set(gca,'ydir','normal')
    xLength = floor((size(im, 2)-1) * px_size/5)*5;
    xTick = linspace(0, xLength, 6);
    set(gca,'XTick', xTick')
    set(gca,'XTickLabel',xTick')
    yLength = floor((size(im, 1)-1) * px_size/5)*5;
    yTick = linspace((size(im, 1)-1) * px_size-yLength, (size(im, 1)-1) * px_size, 6);
    yTickLabel = linspace(yLength, 0, 6);
    set(gca,'YTick', yTick')
    set(gca,'YTickLabel',yTickLabel')
    %     imshow(img)
    %     px_size_old = px_size;
    %     px_size = 1;
    hold on
    if (size(im,3) <3)
        colormap grey
    end
    %     view(0, -90)
    axis off
%     axis tight
%     axis([0 size(im,2)*px_size 0 size(im,1)*px_size])

end
xlabel('\mum')
ylabel('\mum')

zlevel = -100;
for i = 1:length(tl)
%     skip nuclear bodies
    if ~isempty(intersect(nbInd, i))
        continue;
    end
    
    [A, c] = MinVolEllipse(tl{i}.convhull', 0.01);
    p = [];
    for j = 1:length(tl{i}.tracks)
        p = [p; tl{i}.tracks{j}(:,[2 3])];
    end
    p = p * px_size;
    plot(p(:,1), p(:,2), 'Color', 'c', 'LineWidth', 1);
    hold on

    if (~isempty(motionType))
        ellColor = colorspec(motionType(i)+1);
    end

    if ~isempty(ellColor)
        Ellipse_plot(A, c, 20, px_size, ellColor, 2)
    end
    if showStart
        plot(p(1,1), p(1,2), '.r','MarkerSize',20);
    end
    if showEnd
        plot(p(end,1), p(end,2), '.g','MarkerSize',20);
    end
    if isfield(tl{i}, 'id')
        idx = tl{i}.id;
    else
        idx = i;
    end
    if show_ids == 1
%         text(p(1,1)+5* px_size, p(1,2)+5* px_size, sprintf('%d(%d)', i, size(p,1))); 
        text(p(1,1)+0.5* px_size, p(1,2)+0.5* px_size, sprintf('%d', idx)); 
    elseif  show_ids == 2
%         text(p(1,1), p(1,2), sprintf('%d(%d)', i, size(p,1)), 'BackGroundColor', 'White'); 
%         text(p(1,1)+1* px_size, p(1,2)+1* px_size, sprintf('%d', i), 'BackGroundColor', 'White'); 
%         text('VerticalAlignment', 'top ');
%         text('FontSize', 6);
        text(p(1,1)+0.5* px_size, p(1,2)+0.5* px_size, sprintf('%d', idx), 'BackGroundColor', 'White', ...
            'VerticalAlignment', 'top', 'FontSize', 8, 'Margin',0.1)
    end
end

if isempty(im)
    if isempty(axisSize)
        axis equal
    else
        axis(axisSize)
    end
    set(gca,'YDir','reverse');
end

pos = get(gcf, 'position');
set(gcf, 'position', [pos(1:2)-pos(3:4)*zoomFactor*0.5 pos(3:4)*zoomFactor]);




