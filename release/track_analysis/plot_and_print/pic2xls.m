function pic2xls(pic,file,sheet, range, imscale)
% 
% pic2xls Inserts a picture into an Excel file
%
% pic2xls(pic,file,sheet, range, imscale)
%
% pic2xls  : Inserts a picture into an Excel file
%
%       pic:            image file name (.gif,.jpg,.bmp,..etc)
%       file:           Excel file name (new or existing)
%       sheet:          sheet name.
%       range:          excel range
%       imscale:        scale factor for the image
% 
% Example:
%   pic = 'tree.jpg';
%   file = 'file.xls';
%   pic2xls(pic,file,'Sheet1',[0 0 200 200]);

if nargin<5
    imscale = 1;
end

[fpath,fname,fext] = fileparts(file);
if isempty(fpath)
    out_path = pwd;
elseif fpath(1)=='.'
    out_path = [pwd filesep fpath];
else
    out_path = fpath;
end

[ifpath,ifname,ifext] = fileparts(pic);
if isempty(ifpath)
    iout_path = pwd;
elseif fpath(1)=='.'
    iout_path = [pwd filesep ifpath];
else
    iout_path = ifpath;
end

Excel = actxserver('Excel.Application');

if ~exist(file,'file')
        % The following case if file specified and does not exist (Creating New Workbook)
        Workbook = invoke(Excel.Workbooks,'Add');
        new=1;
        set(Excel, 'Visible', 1);
    else
        % The following case if file specified and does exist (Opening Workbook)
        Workbook = invoke(Excel.Workbooks, 'open', [out_path filesep fname fext]);
        new=0;
        set(Excel, 'Visible', 0);
end
    
% Activating Sheet
Sheets = Excel.Worksheets;
sheet = get(Sheets, 'Item', sheet);
invoke(sheet, 'Activate');

% Adding Picture

% Function AddPicture(Filename As String, LinkToFile As MsoTriState,
% SaveWithDocument As MsoTriState, Left As Single, Top As Single, Width As Single, Height As Single) As Shape

im = imread([iout_path filesep ifname ifext]);
imsz = size(im);

ExAct = Excel.Activesheet;
left = ExAct.Range(range).Left;
top = ExAct.Range(range).Top;
invoke(ExAct.Shapes,'AddPicture',[iout_path filesep ifname ifext],0,1,left,top,round(imsz(2)*imscale),round(imsz(1)*imscale));

if ~new
    invoke(Workbook, 'Save');
    invoke(Excel, 'Quit');
end
        
delete(Excel);