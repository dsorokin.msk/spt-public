function createAndSaveStatDirFigures(fname, modelParams, statDir)

ind = [modelParams.motionTypeC]' == 'F';
% ind = 1:length([statDir.dir_ang]);
ang = [statDir.dir_ang]';
[N,X] = hist(ang(ind), [-180:10:180]);
figure;
bar(X,N, 0.8, 'b')
set(gca,'XLim',[-180 180])
set(gca,'XTick',[-180:30:180])
xlabel('angle (degrees)')
ylabel('Number of PNB')
title('Motion direction angle')
if ~isempty(fname)
%     export_fig([fname(1:end-4) '_mot-dir-ang'], '-png');
    set(gcf, 'PaperPosition', get(gcf, 'PaperPosition')*0.5);
    saveas(gcf,[fname(1:end-4) '_mot-dir-ang'], 'png');
end

ang = [statDir.nb_ang_diff]';
[N,X] = hist(ang(ind), [0:10:180]);
figure;
bar(X,N, 0.8, 'k')
set(gca,'XLim',[0 190])
set(gca,'XTick',[0:20:180])
xlabel('angle (degrees)')
ylabel('Number of PNB')
title('NB deviation angle')
if ~isempty(fname)
%     export_fig([fname(1:end-4) '_NB-dev-ang'], '-png');
    set(gcf, 'PaperPosition', get(gcf, 'PaperPosition')*0.5);
    saveas(gcf,[fname(1:end-4) '_NB-dev-ang'], 'png');
end

ang = [statDir.nuc_ang_diff]';
[N,X] = hist(ang(ind), [0:10:180]);
figure;
bar(X,N, 0.8, 'facecolor', [0 0.6 0])
set(gca,'XLim',[0 190])
set(gca,'XTick',[0:20:180])
xlabel('angle (degrees)')
ylabel('Number of PNB')
title('Nucleus center deviation angle')
if ~isempty(fname)
%     export_fig([fname(1:end-4) '_NB-dev-ang'], '-png');
    set(gcf, 'PaperPosition', get(gcf, 'PaperPosition')*0.5);
    saveas(gcf,[fname(1:end-4) '_center-dev-ang'], 'png');
end

end