function plotTracksTime(tl, h)

if nargin<2
    h = figure;
end

if isa(tl, 'containers.Map')
    tl = tl.values;
end

cmap = jet(length(tl));
figure(h);
cla
% hold off;
% plot(NaN);
hold on;
for t=1:length(tl)
    p = [];
    for j=1:length(tl{t}.tracks)
        p = [p ; tl{t}.tracks{j}(:,5)];
    end
    if isfield(tl{t}, 'id')
        idx = tl{t}.id;
    else
        idx = t;
    end
    plot(p(:,1),idx, '.', 'color', cmap(t,:));
    grid on
    xlabel('time')
    ylabel('id')
end

% figure
% hold on;
% for t=1:length(tl)
%     p = [];
%     for j=1:length(tl{t}.tracks)
%         p = [p ; tl{t}.tracks{j}(:,5) tl{t}.tracks{j}(:,3)];
%     end
%     plot(p(:,1),p(:,2), '-', 'color', cmap(t,:));
%     grid on
%     xlabel('time')
%     ylabel('y')
% end
% 
% figure
% hold on;
% for t=1:length(tl)
%     p = [];
%     for j=1:length(tl{t}.tracks)
%         p = [p ; tl{t}.tracks{j}(:,5) tl{t}.tracks{j}(:,2)];
%     end
%     plot(p(:,1),p(:,2), '-', 'color', cmap(t,:));
%     grid on
%     xlabel('time')
%     ylabel('x')
% end