function tl = join_tracks_new_map(tl, id1, id2)
% joins all subtracks of two tracks id1 and id2
% the join is done only if subtracks span different time moments
% if subtracks overlay error is printed

lng1 = length(tl(id1).tracks);
lng2 = length(tl(id2).tracks);

times1 = track_times(tl(id1).tracks);
times1 = [0 0; times1; 100000 100000];

for j = 1:lng2
    % find where to insert subtrack 
    start2 = tl(id2).tracks{j}(1, 5);
    end2 = tl(id2).tracks{j}(end, 5);
    
    % find start index
    id_start = 0;
    for i = 1:length(times1)-1
        if (times1(i,1) < start2) && (times1(i+1, 1) > start2)
            id_start = i - 1;
            break;
        end
    end

    % find end index
    for i = 1:length(times1)-1
        if (times1(i,2) < end2) && (times1(i+1, 2) > end2)
            id_end = i - 1;
            break;
        end
    end
    
    if id_start ~= id_end
        error(sprintf('tracks %d & %d overlap in time!', id1, id2));
    end
    
    if ~isempty(intersect(tl(id1).tracks{j}(2:end-1, 5), tl(id2).tracks{j}(2:end-1, 5)))
        fprintf(['Indexes: ' num2str(id1) ', ' num2str(id2) '\nTimes:']);
        intersect(tl(id1).tracks{j}(:, 5), tl(id2).tracks{j}(:, 5))
        error('tracks overlap in time!');
    end
    
    tr = tl(id1);

    t2 = tl(id2).tracks{j}; % take subtrack for insert
    if tr.tracks{j}(end, 5) == t2(1,5) % avoid multiple timepoints in resulting track
        t2 = t2(2:end,:);
    end
    if tr.tracks{j}(1, 5) == t2(end,5)
        t2 = t2(1:end-1,:);
    end
    ch_idx = convhull(t2(:,2), t2(:,3));
    track_convex_hull = t2(ch_idx,[2 3]);
    
    % insert track
    if id_start == 0 
        tr.tracks = {t2, tr.tracks{:}};        
    else if id_start == lng1 + 1; 
            tr.tracks = {tr.tracks{:}, t2};
        else
            tr.tracks = {tr.tracks{1:id_start}; t2; tr.tracks{id_start+1:end}};
        end
    end  
    
    p = [tr.convhull; track_convex_hull];
    tr.convhull = p(convhull(p(:,1), p(:,2)), [1 2]);       
end
% recompute ellipse params for new track 
[A, c] = MinVolEllipse(tl(id1).convhull', 0.01);
tr.ellipse_A = A;
tr.ellipse_c = c;   

% delete track id2
tl.remove(id2);

for j=2:length(tr.tracks)
    tr.tracks{1} = [tr.tracks{1}; tr.tracks{j}];
end
tr.tracks(2:end) = [];

tl(id1) = tr;
