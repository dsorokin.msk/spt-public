function [tlNew] = semiAutoTrackSplit(spotsOvlImg, tl, jumpLength)

try 
    
imsz = imsize(spotsOvlImg);
seqLength = imsz(3);
imsz = imsz(1:2);

h = dipshow(spotsOvlImg);
dipstep('on')
set(h, 'position', [1985 490 512 512]);
h2 = figure('position', [2652 -113 860 1109]);
h1(1) = subplot(2,1,1);
h1(2) = subplot(2,1,2);

% h2 = figure;
% plot_all_tracks_ellipce(tl([i colliding_tnum]), [], 1, [i colliding_tnum], 1, 25, 100000)

% colliding_tnum = -1;
i = 1;

while (i <= length(tl))
    track = [];
    for j=1:length(tl{i}.tracks)
        track = [track; tl{i}.tracks{j}];
    end
    maxTime = max(track(:,5));
    flags = 0;
    while sum(flags)==0
%         colliding_tnum = findClosestTrafindNonCompactTrackcks(tl, i, 10, 2); 
        [flags, points, times] = findNonCompactTrack(tl, i, jumpLength);
        if sum(flags)==0
            break;
        end
        
%         figure(h2);
        set(0, 'currentfigure', h2)
        subplot(h1(1));
        plot_all_tracks_ellipce(tl, [], 1, 1:length(tl), 1, 25, 100000, [1 imsz(1) 1 imsz(2)])
        hold on
        plot_all_tracks_ellipce(tl([i]), [], 1, [i], 1, 25, 100000, [1 imsz(1) 1 imsz(2)], 0, 'g', [])
        axis tight
        axis equal
        subplot(h1(2));
        plot_all_tracks_ellipce(tl([i]), [], 2, [i], 1, 25, 100000)
        plot(points(:,1), points(:,2), '*g');
        dipshow(h, 'ch_slice', maxTime);
%         dipstep('on');
        while ~isempty(times)
            fprintf('%d; ', [times(end)]);
            fprintf('\n');
            str = input('Enter S if you want to split the track starting from the first timepoint mentioned above OR \nenter C if you want to cut the green points \nOR Enter N if you want to keep everything the same\n', 's');
            if (strcmpi(str, 'n'))
                figure(h2);
                subplot(h1(1))
                hold off
                plot(NaN)
                subplot(h1(2))
                hold off
                plot(NaN)
                break;
            elseif (strcmpi(str, 's'))
                if (times(end)<max(track(:,5))-3) && (times(end)>=min(track(:,5))+3)
                    tl = split_track(tl, i, times(end));
                elseif (times(end)>=max(track(:,5))-3) && times(end)~= max(track(:,5))
                    disp('It is almost the end of the track, so it was cut out');
                    tl = cut_track(tl, i, times(end):max(track(:,5)));
                elseif  times(end)<3
                    disp('It is almost the start of the track, so it was cut out');
                    tl = cut_track(tl, i, min(track(:,5)):times(end));
                end
            elseif (strcmpi(str, 'c'))
    %             if (length(times)>1)
                    tl = cut_track(tl, i, times(end));
    %             else
    %                 tl = cut_track(tl, i, times:times(1)+1);
    %             end
            end
            times(end)=[];
            points(end,:)=[];
            if ~isempty(times)
                track = tl{i}.tracks{1};
                subplot(h1(2));
                hold off
                plot_all_tracks_ellipce(tl([i]), [], 2, [i], 1, 25, 100000)
                hold on
                plot(points(:,1), points(:,2), '*g');
            end
        end
        figure(h2);
        subplot(h1(1))
        hold off
        plot(NaN)
        subplot(h1(2))
        hold off
        plot(NaN)
    end
    i = i+1;
end

close(h2);
close(h)

tlNew = tl;

catch err

% close(h1);
% close(h)

disp(err)

tlNew = tl;

end


end