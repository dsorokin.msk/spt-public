function [tlNew] = semiAutoTrackDelete(spotsOvlImg, tl, trackLength)

try 
    
imsz = imsize(spotsOvlImg);
seqLength = imsz(3);
imsz = imsz(1:2);

h = dipshow(spotsOvlImg);
dipstep('on')
set(h, 'position', [1985 490 512 512]);
h2 = figure('position', [2652 -113 860 1109]);
h1(1) = subplot(2,1,1);
h1(2) = subplot(2,1,2);

% h2 = figure;
% plot_all_tracks_ellipce(tl([i colliding_tnum]), [], 1, [i colliding_tnum], 1, 25, 100000)

% colliding_tnum = -1;
i = 1;

while (i <= length(tl))
    track = [];
    for j=1:length(tl{i}.tracks)
        track = [track; tl{i}.tracks{j}];
    end
    maxTime = max(track(:,5));
    flags = 0;
    if size(track,1) <= trackLength
        figure(h2);
        subplot(h1(1));
        plot_all_tracks_ellipce(tl, [], 1, 1:length(tl), 1, 25, 100000, [1 imsz(1) 1 imsz(2)])
        hold on
        plot_all_tracks_ellipce(tl([i]), [], 1, [i], 1, 25, 100000, [1 imsz(1) 1 imsz(2)], 0, 'g', [])
        axis tight
        axis equal
        subplot(h1(2));
        plot_all_tracks_ellipce(tl([i]), [], 2, [i], 1, 25, 100000)
        dipshow(h, 'ch_slice', maxTime);
        str = input('Delete this track? y/n\n', 's');
        if (strcmpi(str, 'y'))
            tl = delete_tracks(tl, i);
        end
        figure(h2);
        subplot(h1(1))
        hold off
        plot(NaN)
        subplot(h1(2))
        hold off
        plot(NaN)
    end
    i = i+1;
end

close(h2);
close(h)

tlNew = tl;

catch err

% close(h1);
% close(h)

disp(err)

tlNew = tl;

end


end