function tl = delete_tracks(tl, ids_to_delete)

if iscell(tl)
    tl(ids_to_delete) = [];
elseif isa(tl, 'containers.Map')
    for i=1:length(ids_to_delete)
        tl.remove(ids_to_delete(i));
    end
end


