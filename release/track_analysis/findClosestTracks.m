function [colliding_tnum] = findClosestTracks(trackList, trackInd, radius, timeTolerance)

colliding_tnum = [];
i = 1;

track = trackList{trackInd}.tracks{end};
maxTime = max(track(:,5));
xC = track(1,2);
yC = track(1,3);

while (i <= length(trackList))
%     A = trackList{i}.ellipse_A;
%     c = trackList{i}.ellipse_c;
    track = trackList{i}.tracks{1};
    minTime = min(track(:,5));
    x = track(1,2);
    y = track(1,3);
    if (i ~= trackInd) && ((x-xC)^2 + (y-yC)^2 <= radius^2) && maxTime <= minTime-timeTolerance
        colliding_tnum = [colliding_tnum i];
    end
    i = i + 1;
end

end