function sse=MSDfitFlow(params,Input,Actual_Output)
% 4*sigma^2 + 4*D*t + v^2*t^2 - flow diffusion
    D = params(1);
    v = params(2);
    error = Actual_Output(1);
    Fitted_Curve=error + 4*D.*Input + v^2*Input.^2;
    Error_Vector=Fitted_Curve - Actual_Output;
    % When curvefitting, a typical quantity to
    % minimize is the sum of squares error
    sse=sum(Error_Vector.^2)/length(Error_Vector);
end