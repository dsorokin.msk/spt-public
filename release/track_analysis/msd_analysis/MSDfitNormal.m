function sse=MSDfitNormal(params,Input,Actual_Output)
% 4*sigma^2 + 4*D*t^alpha - Brownian motion and anomalous diffusion
    D = params(1);
%     alpha = params(2);
    error = Actual_Output(1);
%     Fitted_Curve=error + 4*D.*(Input.^alpha);
    Fitted_Curve=error + 4*D.*(Input);
    Error_Vector=Fitted_Curve - Actual_Output;
    % When curvefitting, a typical quantity to
    % minimize is the sum of squares error
    sse=sum(Error_Vector.^2)/length(Error_Vector);
end