function [res, estParams, minInd] = lsf_fit_msd_all_models(msd, delta_t, px_size)
% fits measured MSD curve using LSF with the following models and choose the best fit:
% 4*sigma^2 + 4*D*t^alpha - Brownian motion and anomalous diffusion
% 4*sigma^2 + 4*D*t + v^2*t^2 - flow diffusion
% 4*sigma^2 + 2*L^2(1-exp(-4*D_micro*t/(2*L^2))) + 4*L_macro*t - walking confined
% 
% for mathematical details please refer to [X. Michalet, "MSD analysis...", 2010]
% and other papers I used
% 
% synopsis:
% [res, modelParams] = lsf_fit_msd_all_models(msd, delta_t, px_size)
% inputs:
% msd -         measured MSD curve
% delta_t -     Time Lag (in sec)
% px_size -     pixel size
% 
% outputs:
% res -         estimated MSD
% estD -        estimated diffusion coefficient
% estAlpha -    estimated alpha
% 

    startAlpha = 1;
    startD = (msd(2)-msd(1))/delta_t/4;
    if (nargin<2)
        delta_t = 1;
    end

%   coefficient to increase the computation accuracy
    if (startD < 1e-4)
        compC = 10000;
    else
        compC = 1;
    end

    t=1:length(msd);
    t=t(:) * delta_t;
    msd = msd(:) * compC;
    

% Normal
%     Starting=[startD*compC startAlpha startSigma*sqrt(compC)];
%     Starting=[startD*compC startAlpha];
    Starting=[startD*compC];
    options=optimset('Display','off');
    [Estimates1,errorNorm1,exitflag1] = fminsearch(@MSDfitNormal,Starting,options,t,msd);
    estParams{1}.D = Estimates1(1)/compC;
%     estAlpha1 = Estimates1(2);
    estParams{1}.secPar = 1;
%     estSigma = Estimates(3)/sqrt(compC);
    estParams{1}.sigma = sqrt(msd(1)/compC)/2;
    estParams{1}.res = 4*estParams{1}.sigma^2 + 4*estParams{1}.D.*(t.^estParams{1}.secPar);
    estParams{1}.errorNorm = errorNorm1;
    if (abs(estParams{1}.secPar-1) <= 0.10)
        estParams{1}.type = 'Normal';
    elseif (estParams{1}.secPar < 0.90)
        estParams{1}.type = 'Anomaluos';
    end
    estParams{1}.motionType = 0;
    estParams{1}.motionTypeC = 'N';
    
% Flow    
    startV = 0.015;
    Starting=[startD*compC startV*compC];
    options=optimset('Display','off','MaxFunEvals', 10000, 'MaxIter', 10000);
%     [Estimates2,errorNorm2,exitflag2] = fminsearchbnd(@MSDfitFlow,Starting,[0 100], [0 100], options,t,msd);
    [Estimates2,errorNorm2,exitflag2] = fminsearch(@MSDfitFlow,Starting, options,t,msd);
    estParams{2}.D = Estimates2(1)/compC;
    estParams{2}.secPar = Estimates2(2)/compC; % v parameter
%     estSigma = Estimates(3)/sqrt(compC);
    estParams{2}.sigma = sqrt(msd(1)/compC)/2;
    estParams{2}.res = 4*estParams{2}.sigma^2 + 4*estParams{2}.D.*t + estParams{2}.secPar^2*t.^2;
    estParams{2}.errorNorm = errorNorm2;
    estParams{2}.type = 'Flow';
    estParams{2}.motionType = 1;
    estParams{2}.motionTypeC = 'F';
    
% Confined    
    startA = msd(1);
    Starting=[startD*compC startA];
    options=optimset('Display','off','MaxFunEvals', 10000, 'MaxIter', 10000);
%     [Estimates3,errorNorm3,exitflag3] = fminsearchbnd(@MSDfitWalkingConfined,Starting,[0 inf], [0 inf], options,t,msd);
    [Estimates3,errorNorm3,exitflag3] = fminsearch(@MSDfitConfined,Starting,options,t,msd);
%     estD_macro = Estimates3(1)/compC;
    estParams{3}.D = Estimates3(1)/compC;
    estParams{3}.secPar = Estimates3(2);
%     estSigma = Estimates(3)/sqrt(compC);
    estParams{3}.sigma = sqrt(msd(1)/compC)/2;
    estParams{3}.res = 4*estParams{3}.sigma^2 + estParams{3}.secPar*(1-exp(-4*estParams{3}.D*t/(estParams{3}.secPar)));% + 4*estD_macro*t;
    estParams{3}.errorNorm = errorNorm3;
    estParams{3}.type = 'Confined';
    estParams{3}.motionType = 2;
    estParams{3}.motionTypeC = 'C';
    
% % Walking confined
%     % Confined    
%     startA = msd(1);
%     Starting=[startD*compC startD*compC startA];
%     options=optimset('Display','on', 'MaxFunEvals', 10000, 'MaxIter', 10000);
%     [Estimates3,errorNorm3,exitflag3] = fminsearch(@MSDfitWalkingConfined,Starting, options,t,msd);
% %     [Estimates3,errorNorm3,exitflag3] = fminsearch(@MSDfitConfined,Starting,options,t,msd);
% %     estD_macro = Estimates3(1)/compC;
%     estParams{3}.D = Estimates3(2)/compC;
%     estParams{3}.secPar = Estimates3(3);
%     estParams{3}.thrdPar = Estimates3(1)/sqrt(compC);
% %     estSigma = Estimates(3)/sqrt(compC);
%     estParams{3}.sigma = sqrt(msd(1)/compC)/2;
%     estParams{3}.res = 4*estParams{3}.sigma^2 + estParams{3}.secPar*(1-exp(-4*estParams{3}.D*t/(estParams{3}.secPar))) + 4*estParams{3}.thrdPar*t;
%     estParams{3}.errorNorm = errorNorm3;
    
    
%     figure
%     plot((1:length(msd))*delta_t, msd/compC, '.k');
%     hold on
%     plot((1:length(res1))*delta_t, res1,'--g');
%     plot((1:length(res2))*delta_t, res2,'--c');
%     plot((1:length(res3))*delta_t, res3,'--k');
    
    [~, minInd] = min([errorNorm1 errorNorm2 errorNorm3]);
    switch minInd
        case 1
            
        case 2
            if abs(errorNorm2-errorNorm1) < 0.1*0.5*(errorNorm2+errorNorm1) || ...
                estParams{2}.secPar < 0.02 && 0.5*errorNorm1 < errorNorm2
                minInd = 1;
            end
        case 3
%             if (estParams{3}.secPar > msd(end)*5.5)
            if abs(errorNorm3-errorNorm1) < 0.1*0.5*(errorNorm3+errorNorm1) || ...
                 estParams{3}.secPar > msd(end)*5
                minInd = 1;
            end 
    end
    
%     convert everything to MICRONS
    for i=1:3
        estParams{i}.D = estParams{i}.D * px_size * px_size;
        estParams{i}.sigma = estParams{i}.sigma * px_size * px_size;
        estParams{i}.res = estParams{i}.res * px_size * px_size;
        estParams{i}.secPar = estParams{i}.secPar * px_size * px_size;
    end
    
    modelParams = estParams{minInd};
    res = estParams{minInd}.res;
    
    
end