% comp_track_stat(tracks)
% computes statisticts about tracks
% tracks ....... computed using t = create_tracks
% delta_t ...... span between frames in seconds (default 2.603)
% px_size ...... lateral pixel size in nm (default 30)
% seq_length ... original sequence length (in frames)
% print ........ if = 1 statistics is also printed
function [msd, motionType, fittedMSD, modelParams] = computeTrackMSD(tracks, delta_t, px_size, seq_length, max_msd_time_lag, msdpath)

if nargin<6
    msdpath = [];
end

dispMSD = ~isempty(msdpath);

% [fpath, fnameNE, ~] = fileparts(msdPath);
% fpath = [fpath filesep];

% allocate stat array, rows are trajectories
msd = cell(length(tracks), 1);
motionType = zeros(length(tracks),1);
for i = 1:length(tracks)
    if length(tracks{i}.tracks)>1
        error(['more than 1 subtrack in track number ' num2str(i)])
    end

    % MSD
    msd{i} = comp_trajectory_msd(tracks{i}.tracks, max_msd_time_lag, px_size, round(seq_length));
    
%     msd=msd*px_size*px_size; % switch to microns
    % diffusion coef
%     [res,D,alpha] = lsf_fit_msd(msd{i}, delta_t);
%     D = D*1000000; % in nm^2/s
%     [motionType(i), motionTypeC] = getMotionType(alpha);

    if i==17
        aaa = 0;
    end

    [res, estParams, minInd] = lsf_fit_msd_all_models(msd{i}, delta_t, px_size);
%     [res, estParams, minInd] = lsf_fit_msd_all_models_AIC(msd{i}, delta_t);
%     convert MSD to MICRONS
    msd{i} = msd{i} * px_size * px_size;
    modParams = estParams{minInd};
    if dispMSD
        plotSimpleMSDfitResult(msd{i}, minInd, estParams)
        title(sprintf('MSD{%d}', i));
%         msdpath = 'msd';
        if ~exist(msdpath, 'dir')
            mkdir(msdpath);
        end        
        export_fig([msdpath filesep 'track' sprintf('%.2d', i)], '-png');
        close
        % saving the raw data
        if ~exist([msdpath filesep 'data'], 'dir')
            mkdir([msdpath filesep 'data']);
        end        
        f1 = fopen([msdpath filesep 'data' filesep 'track' sprintf('%.2d', i) '.txt'], 'w');
        fprintf(f1, '%.3f\t%.16f\t%.16f\n', [[1:length(msd{i})]' msd{i} estParams{minInd}.res]');
        fclose(f1);
    end
%     D = modParams.D*1000000*px_size*px_size; % in nm^2/s
%     alpha = modParams.secPar*px_size;
%     D = modParams.D;
%     alpha = modParams.secPar;    
    motionType(i) = modParams.motionType;
%     motionTypeC = modParams.motionTypeC;
    fittedMSD{i} = res; 
    modParams.msd = msd{i};
    modParams.fittedMSD = fittedMSD{i};
    modelParams(i) = modParams;
end

