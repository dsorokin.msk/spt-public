function sse=MSDfitConfined(params,Input,Actual_Output)
% 4*sigma^2 + A*(1-exp(-4*D_micro*t/(A))) + 4*D_macro*t - walking confined
%     D_macro = params(1);
    D_micro = params(1);
    A = params(2);
    error = Actual_Output(1);
%     Fitted_Curve=error + A*(1-exp(-4*D_micro*Input/(A))) + 4*D_macro*Input;
    Fitted_Curve=error + A*(1-exp(-4*D_micro*Input/(A)));% + 4*0*Input;
    Error_Vector=Fitted_Curve - Actual_Output;
    % When curvefitting, a typical quantity to
    % minimize is the sum of squares error
    sse=sum(Error_Vector.^2)/length(Error_Vector);
end