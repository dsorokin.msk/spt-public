function msd = comp_trajectory_msd(trajectory, max_time_lag, px_size, seq_length)
% computes trajectory MSD according to formula (31) in [X. Michalet 2010]
% trajectory ..... may consists from several subtrajetories
% max_time_lag ... defines the length of the result (how many time lags to assume)
% px_sixe ........ coordinates are in pixels. This value says the size of
%                  one pixel in nm.

known = zeros(seq_length, 1);
x = zeros(seq_length, 1);
y = zeros(seq_length, 1);
msd = [];

for i = 1:length(trajectory)
    known(trajectory{i}(:,5)) = 1;
    
    % convert coordinates to microns
    x(trajectory{i}(:,5)) = px_size * trajectory{i}(:,2) / 1000;
    y(trajectory{i}(:,5)) = px_size * trajectory{i}(:,3) / 1000;
end

% compute formula (31)
for i = 1:max_time_lag
    valid = known(1:end-i) & known(1 + i:end);
    x2 = (x(1:end-i) - x(1+i:end)).^2 .* valid;
    y2 = (y(1:end-i) - y(1+i:end)).^2 .* valid;
    
    if sum(valid) == 0
        break;
    end
        
    msd = [msd; 1/(sum(valid)) * sum(x2 + y2)];    
end

