function tl = join_more_tracks(tl, tids)
% join tracks from 'tl' with indexes 'tids' into one

tids = sort(tids);
first_id = tids(1);
ids = tids(2:end);

if isempty(ids)
    error('At least two ids required.');
end

for i = 1:length(ids)
    if iscell(tl)
        tl = join_tracks_new(tl, first_id, ids(i));
        ids = ids - 1; % join_tracks removed one track
    elseif isa(tl, 'containers.Map')
        tl = join_tracks_new_map(tl, first_id, ids(i));
    end
end