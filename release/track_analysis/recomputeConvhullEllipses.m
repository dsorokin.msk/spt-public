function tl1 = recomputeConvhullEllipses(tl)

tlRaw = tl;
if isa(tlRaw, 'containers.Map')
    tl = tlRaw.values;
end

for i=1:length(tl)
    tr = [];
    for j=1:length(tl{i}.tracks)
        tr = [tr; tl{i}.tracks{j}];
    end
    ch_idx = convhull(tr(:,2), tr(:,3));
    tl{i}.convhull = tr(ch_idx,2:3);
    [A, c] = MinVolEllipse(tl{i}.convhull', 0.01);
    tl{i}.ellipse_A = A;
    tl{i}.ellipse_c = c;  
end

tl1 = tl;
if isa(tlRaw, 'containers.Map')
    ks = tlRaw.keys;
    for i=1:length(ks)
        tlRaw(ks{i}) = tl1{i};
    end
    tl1 = tlRaw;
end

end