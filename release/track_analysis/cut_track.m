function tl = cut_track(tl, idx, times)

if isa(tl, 'containers.Map')
    tr = tl(idx).tracks{1};
elseif iscell(tl)
    tr = tl{idx}.tracks{1};
end

ind1 = find(tr(:,5) == times(1), 1, 'first');
if length(times)>1
    ind2 = find(tr(:,5) == times(2), 1, 'first');
    tr(ind1:ind2,:)=[];
else
    tr(ind1,:)=[];
end
if isa(tl, 'containers.Map')
    tmp = tl(idx);
    tmp.tracks{1} = tr;
    tl(idx) = tmp;
elseif iscell(tl)
    tl{idx}.tracks{1} = tr;
end

tl = recomputeConvhullEllipses(tl);