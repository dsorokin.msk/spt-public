function tlNew = semiAutoTrackJoin(spotsOvlImg, tl, radius, timeTolerance)

try 
    
imsz = imsize(spotsOvlImg);
seqLength = imsz(3);
imsz = imsz(1:2);



h = dipshow(spotsOvlImg);
dipstep on;
set(h, 'position', [1985 490 512 512]);
h2 = figure('position', [2652 -113 860 1109]);
h1(1) = subplot(2,1,1);
h1(2) = subplot(2,1,2);
h3 = figure('position', [2029 -74 560 420]);
% h2 = figure;
% plot_all_tracks_ellipce(tl([i colliding_tnum]), [], 1, [i colliding_tnum], 1, 25, 100000)

% colliding_tnum = -1;
i = 1;
while (i <= length(tl))
    track = tl{i}.tracks{1};
    maxTime = max(track(:,5));
    colliding_tnum = -1;
    while ~isempty(colliding_tnum)
        colliding_tnum = findClosestTracks(tl, i, radius, timeTolerance); 
        if isempty(colliding_tnum)
            break;
        end
        
%         figure(h2);
        set(0, 'currentfigure', h2)
        subplot(h1(1));
        plot_all_tracks_ellipce(tl, [], 1, 1:length(tl), 1, 25, 100000, [1 imsz(1) 1 imsz(2)])
        hold on
        plot_all_tracks_ellipce(tl([i colliding_tnum]), [], 1, [i colliding_tnum], 1, 25, 100000, [1 imsz(1) 1 imsz(2)], 0, 'g', [])
        axis tight
        axis equal
        subplot(h1(2));
        plot_all_tracks_ellipce(tl([i colliding_tnum]), [], 2, [i colliding_tnum], 1, 25, 100000)
        plotTracksTime(tl([i colliding_tnum]), [i colliding_tnum], h3);
        dipshow(h, 'ch_slice', maxTime);
%         dipstep('on');
        fprintf('%d; ', [i colliding_tnum]);
        fprintf('\n');
        str = input('Enter Y if you want to join the tracks with numbers given in the previous line OR \nenter N if you want to pass to the next track \nOR Enter the numbers to join delimited by spaces\n', 's');
        if (strcmpi(str, 'n'))
            figure(h2);
            subplot(h1(1))
            hold off
            plot(NaN)
            subplot(h1(2))
            hold off
            plot(NaN)
            break;
        elseif (strcmpi(str, 'y'))
            indToJoin = [i colliding_tnum];
        else
            c = strsplit(str);
            indToJoin = [];
            for j=1:length(c)
                indToJoin = [indToJoin str2num(c{j})];
            end
        end
        indToJoin = sort(unique(indToJoin));
        if (indToJoin(1) ~= i)
            indToJoin = [i indToJoin];
        end
        tl = join_more_tracks(tl, [indToJoin]);
        figure(h2);
        subplot(h1(1))
        hold off
        plot(NaN)
        subplot(h1(2))
        hold off
        plot(NaN)
    end
    i = i+1;
end

close(h2);
close(h)
close(h3)

tlNew = tl;

catch err

% close(h1);
% close(h)

disp(err)

tlNew = tl;

end



end