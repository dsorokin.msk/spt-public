function [track_list] = create_tracks_fromTrackArray(min_length, track_array)
% converts tracks from ICY track array to matlab track list
% 
% synopsys:
% [track_list] = create_tracks_fromTrackArray(min_length, track_array)
% 
% inputs:
% min_length -        minimum track length
% track_array -       array of tracks
% 
% outputs:
% track_list -        output track list
% 

if nargin < 3
    pout = 'out';
end

d{1} = pout;

t = track_array;

% we get tracks one after the other
t = sortrows(t,1);

% "split" tracks into cell array and remove the short ones
tid = t(1,1);
track_list = {};
t1 = [];
for i = 1:size(t,1)
    if tid == t(i,1)
        t1 = [t1; t(i,:)];
    else
        tid = t(i,1);
        if size(t1,1) > min_length % store long enough tracks only...
            track_list = add_subtrack(track_list, t1);
        end
        t1 = t(i,:);
    end
end

if size(t1,1) > min_length % store long enough tracks only...
    track_list = add_subtrack(track_list, t1);
end

% cd(cwd);

end

function track_list = add_subtrack(track_list, t1)
            % check if the new track starts in a minimal enclosed ellipse
            % of any previous track. If yes, link them.
            if (size(t1,1) > 2) % if there are less than 2 point convhull doesn't work
                ch_idx = convhull(t1(:,2), t1(:,3));
                track_convex_hull = t1(ch_idx,[2 3]);
                start_time = t1(1,5);

%                 [colliding_tnum] = find_colliding_track(track_list, track_convex_hull, start_time);
                colliding_tnum=-1;
            else
                track_convex_hull = [t1(:,2) t1(:,3)];
%                 track_convex_hull = [track_convex_hull; track_convex_hull];
                colliding_tnum = -1;
            end
            %[colliding_tnum] = find_track_by_xy(track_list, t1(1,2), t1(1, 3));
            if (colliding_tnum == -1)
                % add new track
                tnum = length(track_list) + 1;
                track_list{tnum}.tracks{1} = t1;
                track_list{tnum}.convhull = track_convex_hull;
            else 
                % append to existing track
                tnum = colliding_tnum;
                track_list{tnum}.tracks{end+1} = t1;
                p = [track_list{tnum}.convhull; track_convex_hull];
                track_list{tnum}.convhull = p(convhull(p(:,1), p(:,2)), [1 2]);
            end
            % recompute enclosing ellipse
            [A, c] = MinVolEllipse(track_list{tnum}.convhull', 0.01);
            track_list{tnum}.ellipse_A = A;
            track_list{tnum}.ellipse_c = c;   
end