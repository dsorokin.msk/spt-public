function [flags, points, times] = findNonCompactTrack(tl, i, jumpLength)

tracks = tl{i}.tracks;
aa = [];
for t=1:length(tracks)
    aa = [aa; tracks{t}];
end
difx = diff(aa(:,2));
dify = diff(aa(:,3));
flags = difx.^2 + dify.^2 > jumpLength^2;

points = aa(flags,2:3);
times = aa(flags,5);

end