function spots = tracks2spots(tl)

maxL = 0;
for i=1:length(tl)
    maxL = max([maxL max(tl{i}.tracks{1}(:,5))]);
end
for i=1:maxL
    spots{i} = [];
end

for i=1:length(tl)
    t = tl{i}.tracks{1};
    for j=1:size(t,1)
        spots{t(j,5)} = [spots{t(j,5)}; t(j,2:3) i t(j,1) t(j,4:end)];
    end
end