function tlNew = addAreaInfo(ics, tl)

seqLength = imsize(ics,3);
fprintf(['Adding areas. Overall ' num2str(seqLength-1) ' slices. Progress: ']);
for i=0:seqLength-1
    s = slice_ex(ics, i);
    m = measure(s, s*2, {'Center', 'Size'});
    for j=1:length(tl)
        if (length(tl{j}.tracks{1}) < i+1)
            continue;
        end
        t = tl{j}.tracks{1}(i+1,:);
        dist = pdist([t(2:3); m.Center']);
        [~,mi] = min(dist(1:length(m)));
        tl{j}.tracks{1}(i+1,6) = m(mi).Size;
    end
    dispProgressCounter(i,0)
end
fprintf('\n');

tlNew = tl;

end