function tl = spots2tracks(spots, tlOld)

tl = tlOld;
for j=1:length(spots)
    for i=1:size(spots{j},1)
        ind = tl{spots{j}(i,3)}.tracks{1}(:,5) == j;
        tl{spots{j}(i,3)}.tracks{1}(ind,2:3) = spots{j}(i,1:2);
    end
end
tl = recomputeConvhullEllipses(tl);
