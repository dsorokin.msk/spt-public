function tl = join_tracks_new(tl, id1, id2)
% joins all subtracks of two tracks id1 and id2
% the join is done only if subtracks span different time moments
% if subtracks overlay error is printed

lng1 = length(tl{id1}.tracks);
lng2 = length(tl{id2}.tracks);

times1 = track_times(tl{id1}.tracks);
times1 = [0 0; times1; 100000 100000];

for j = 1:lng2
    % find where to insert subtrack 
    start2 = tl{id2}.tracks{j}(1, 5);
    end2 = tl{id2}.tracks{j}(end, 5);
    
    % find start index
    id_start = 0;
    for i = 1:length(times1)-1
        if (times1(i,1) < start2) && (times1(i+1, 1) > start2)
            id_start = i - 1;
            break;
        end
    end

    % find end index
    for i = 1:length(times1)-1
        if (times1(i,2) < end2) && (times1(i+1, 2) > end2)
            id_end = i - 1;
            break;
        end
    end
    
    if id_start ~= id_end
        error('tracks overlap in time!');
    end
    
    if ~isempty(intersect(tl{id1}.tracks{j}(2:end-1, 5), tl{id2}.tracks{j}(2:end-1, 5)))
        fprintf(['Indexes: ' num2str(id1) ', ' num2str(id2) '\nTimes:']);
        intersect(tl{id1}.tracks{j}(:, 5), tl{id2}.tracks{j}(:, 5))
        error('tracks overlap in time!');
    end

    t2 = tl{id2}.tracks{j}; % take subtrack for insert
    if tl{id1}.tracks{j}(end, 5) == t2(1,5) % avoid multiple timepoints in resulting track
        t2 = t2(2:end,:);
    end
    if tl{id1}.tracks{j}(1, 5) == t2(end,5)
        t2 = t2(1:end-1,:);
    end
    ch_idx = convhull(t2(:,2), t2(:,3));
    track_convex_hull = t2(ch_idx,[2 3]);
    
    % insert track
    if id_start == 0 
        tl{id1}.tracks = {t2, tl{id1}.tracks{:}};        
    else if id_start == lng1 + 1; 
            tl{id1}.tracks = {tl{id1}.tracks{:}, t2};
        else
            tl{id1}.tracks = {tl{id1}.tracks{1:id_start}, t2, tl{id1}.tracks{id_start+1:end}};
        end
    end  
    
    p = [tl{id1}.convhull; track_convex_hull];
    tl{id1}.convhull = p(convhull(p(:,1), p(:,2)), [1 2]);       
end
% recompute ellipse params for new track 
[A, c] = MinVolEllipse(tl{id1}.convhull', 0.01);
tl{id1}.ellipse_A = A;
tl{id1}.ellipse_c = c;   

% delete track id2
if (id2 == length(tl{id2}))
    tl = tl{1:end-1};
else if id2 == 1
        tl = tl{2:end};
    else
        tl = {tl{1:(id2 - 1)}, tl{(id2 + 1):end}};
    end
end

for j=2:length(tl{id1}.tracks)
    tl{id1}.tracks{1} = [tl{id1}.tracks{1}; tl{id1}.tracks{j}];
end
tl{id1}.tracks(2:end) = [];

