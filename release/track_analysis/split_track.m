function tl = split_track(tl, tid, timepoint)
% splits the track into 2 subtracks
% 
% synopsys:
% tl = split_track(tl, tid, timepoint)
% 
% inputs:
% tl -			is track list
% tid -			id to split
% timepoint -	timepoints of split
% 
% outputs:
% tl -	        output track list
% 

tr = tl{tid}.tracks{1};
ind1 = find(tr(:,5) == timepoint, 1, 'first');
tr_new = tr(ind1:end,:);
tr(ind1:end,:)=[];

% recompute convexhull for the new track
tl{end+1}.tracks{1} = tr_new;
ch_idx = convhull(tr_new(:,2), tr_new(:,3));
tl{end}.convhull = tr_new(ch_idx,[2 3]);

% recompute convexhull for the splitted track
tl{tid}.tracks{1} = tr;
ch_idx = convhull(tr(:,2), tr(:,3));
tl{tid}.convhull = tr(ch_idx,[2 3]);

% recompute ellipse params for the new and splitted tracks
[A, c] = MinVolEllipse(tl{tid}.convhull', 0.01);
tl{tid}.ellipse_A = A;
tl{tid}.ellipse_c = c;  

[A, c] = MinVolEllipse(tl{end}.convhull', 0.01);
tl{end}.ellipse_A = A;
tl{end}.ellipse_c = c;  

end