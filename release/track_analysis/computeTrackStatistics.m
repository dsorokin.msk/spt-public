function [stat] = computeTrackStatistics(tracks, delta_t, px_size, seq_length)
% computes statistics of the tracks
% 
% synopsys:
% [stat] = computeTrackStatistics(tracks, delta_t, px_size, seq_length)
% 
% inputs:
% tracks -        tracks computed using create_tracks
% delta_t -       span between frames in seconds
% px_size -       lateral pixel size in nm
% seq_length -    original sequence length (in frames)
% 
% outputs:
% stat -        array with trajectory statistics
% 

% allocate stat array, rows are trajectories
stat = [];
for i = 1:length(tracks)
    if length(tracks{i}.tracks)>1
        error(['more than 1 subtrack in track number ' num2str(i)])
    end

    % area of enclosing ellipse
    stat(i).areaEll = pi/sqrt(det(tracks{i}.ellipse_A)) * px_size * px_size;
    
    % number of frames where we know trajectory points
    stat(i).num_frames = 0;
    % total distance of the trajectory
    stat(i).total_dist = 0;
    stat(i).lr = [];
    stat(i).dists = [];
    stat(i).areas = [];
     
    % process all sub-trajectories
    jj = 1;
    if (size(tracks{i}.tracks{jj}, 2) >= 5)
        st_frame_from = tracks{i}.tracks{jj}(1, 5);
        st_frame_to = tracks{i}.tracks{jj}(end, 5);
    else
        st_frame_from = 0;
        st_frame_to = 0;
    end
        
    % number of frames in subtrack
    stat(i).num_frames = size(tracks{i}.tracks{jj}, 1);

    % distance
    st_vecs = diff(tracks{i}.tracks{jj}(:,[2 3]));
%         st_dists = px_size * sqrt(sum(st_vecs.*st_vecs, 2)) / 1000; % in nm
    dists = px_size * sqrt(sum(st_vecs.*st_vecs, 2)); % in micron

    % area
    if (size(tracks{i}.tracks{jj},2) > 5)
        areas = px_size * px_size * tracks{i}.tracks{jj}(:,[6]);% in micron^2
    end

    % starting point of trajectory
    stat(i).start_x = tracks{i}.tracks{1}(1, 2);
    stat(i).start_y = tracks{i}.tracks{1}(1, 3);

    % extrapolation of trajectory length
    stat(i).dist_in_whole = mean(dists)*seq_length;
    stat(i).avg_dist = mean(dists);
    stat(i).std_dist = std(dists);
    
    % trajectory distance
    stat(i).total_dist = sum(dists);

    % average velocity
    stat(i).avg_veloc = stat(i).total_dist/delta_t/stat(i).num_frames;
    
    %avg area
    if (size(tracks{i}.tracks{jj},2) > 5)
        stat(i).areas = areas;
        stat(i).avg_area = max(areas);
        stat(i).std_area = std(areas);
    else
        stat(i).avg_area = NaN;
        stat(i).std_area = NaN;
    end
end

