function t = track_times(track)
% return start and end times of subtracks
% 
% synopsys:
% t = track_times(track)
% 
% inputs:
% track -		list of subtracks
% 
% outputs:
% t -	        start and end times
% 

lng = length(track);
t = zeros(lng, 2);

for i = 1:lng
    t(i, 1) = track{i}(1, 5);   % subtrack start time
    t(i, 2) = track{i}(end, 5); % subtrack end time
end
