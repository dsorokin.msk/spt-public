function [pvec, smask]  = algo_find_points_tresh(img, tr, cellM, sizeTr, trMagn, openR, contSmSigma, contSmThr, sigma)
% finds spots in the given image
% 
% synopsys:
% [pvec, pimg, smask]  = algo_find_points_tresh(img, tr, cellM, sizeTr, trMagn)
% 
% inputs:
% img -         input image
% tr -          threshold
%               (parameter for spot detection)
% cellM -       cell body mask, used to suppress spots outside of cell
%               body
% sizeTr -      threshold for spot size (only spots with size>sizeTr remain)
%               if sizeTr has 2 values, then spots with size>sizeTr(1) &
%               size<sizeTr(2) remain
% trMagn -      magnification of threshold found by triangle method. default
%               value is 1 (means using 'triangle' threshold)
% openR -       opening radius
% contSmSigma - contour smoothing sigma
% contSmThr -    contour smoothing threshold
% 
% outputs:
% pvec -        coordinates of centroids of found spots and their areas
%               structure: [x y area]
% pimg -        binary image with points in coordinates of found spots
%               centroids
% smask -       binary image with mask of found spots
% 

% set sizeTr value for correct size filtering
if (length(sizeTr) < 2)
    sizeTr(2)=0;
end

% preprocess
b = stretch(gaussf(medif(img,0), sigma));

% use the threshold modified by trMagn coefficient to ge the mask
smask = threshold(b, 'fixed', tr*trMagn);
smask = opening(smask, openR);
smask = gaussf(uint8(smask), contSmSigma)>contSmThr;

% smask = fillholes(smask);
% multiply by cell body mask to remove outliers
if (~isempty(cellM))
    smask = cellM * smask;
end
% filter by size
smask = dip_image(label(smask, inf, sizeTr(1), sizeTr(2)), 'bin');
% get centroids
m = measure(label(smask), img, {'Gravity', 'Size'});
% Create pointset
if ~isempty(m)
    pvec = m.Gravity';
%     pimg = coord2image(round(pvec), imsize(img));
    % add areas to pvec
    pvec = [pvec m.Size'];
else
    pvec = [];
%     pimg = newim(imsize(img));
end

end