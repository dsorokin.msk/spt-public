function [spots, smaskIcs, ovlIcs] = findSpotsIcs(ics, cellm, par)
% find spots wraper that starts find_spots or loads the data from files
% 
% synopsys:
% [spots, smaskIcs, ovlIcs] = findSpotsIcs(ics, cellm, par, fname)
% 
% inputs:
% ics -         input ST stack of images
% cellm -       ST stack of body mask of input images
% par -         structure with parameters
% fname -       filename of the initial stack
% 
% outputs:
% spots -       cell array of coordinates of centroids of found spots
% smaskIcs -    ST stack of spots mask images
% ovlIcs -      ST stack of overlay of input image and mask of spots
% 
spots = [];
smaskIcs = [];
ovlIcs = [];

t = tic;

% crop the image not to process the rest of the data
if (par.CropToMask)
    icsInit = ics;
    maxproj = dip_image(sum(int8(cellm),3))>0;
    maxproj = dip_image(label(maxproj, Inf, 100), 'bin');
    m = measure(maxproj, maxproj*255, {'minimum','maximum'});
    minX = max(0, m(1).Minimum(1)-5);
    minY = max(0, m(1).Minimum(2)-5);
    maxX = min(imsize(cellm,1), m(1).Maximum(1)+5);
    maxY = min(imsize(cellm,2), m(1).Maximum(2)+5);
    if size(cellm,3)>1
        cellm = cellm(minX:maxX, minY:maxY, :);
    else
        cellm = cellm(minX:maxX, minY:maxY);
    end
    ics = ics(minX:maxX, minY:maxY, :);
end

[spots, smaskIcs, ovlIcs] = find_spots(ics, cellm, par.sigma, ...
                            par.sizeTr, par.trMagn, par.numOfLvl, par.LocalMax, 1);
                        
if (par.CropToMask) && (sum(imsize(ovlIcs) ~= imsize(icsInit)))
%     ovlIcs = extend(ovlIcs, imsize(ovlIcs) + [minX minY 0], 'bottomright');
    if ~par.LocalMax
        s = slice_op('berosion', smaskIcs, 1);
        smaskIcs = smaskIcs-s;
    else
        smaskIcs = slice_op('bdilation', smaskIcs, 2);
    end
    smaskIcs = extend(smaskIcs, imsize(smaskIcs) + [minX minY 0], 'bottomright');
%         cellm = extend(cellm, imsize(cellm) + [minX minY 0], 'bottomright');
%     ovlIcs = extend(ovlIcs, imsize(cellmInit), 'topleft');
    smaskIcs = extend(smaskIcs, imsize(icsInit), 'topleft');
    ovlIcs = overlay(icsInit, smaskIcs, [255 0 0]);
%         cellm = extend(cellm, imsize(ics), 'topleft');
    for j=1:length(spots)
        spots{j} = spots{j} + repmat([minX minY 0], size(spots{j},1),1);
    end
end
                        
disp('Elasped time for spot detection:');
toc(t);
disp(['Elapsed time is ' num2str(toc(t)/60) ' minutes.']);

end