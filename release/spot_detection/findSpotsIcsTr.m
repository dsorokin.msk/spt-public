function [spots, smaskIcs, ovlIcs] = findSpotsIcsTr(ics, cellm, par)
% find spots wraper that starts find_spots or loads the data from files
% 
% synopsys:
% [spots, smaskIcs, ovlIcs] = findSpotsIcs(ics, cellm, par, fname)
% 
% inputs:
% ics -         input ST stack of images
% cellm -       ST stack of body mask of input images
% par -         structure with parameters
% fname -       filename of the initial stack
% 
% outputs:
% spots -       cell array of coordinates of centroids of found spots
% smaskIcs -    ST stack of spots mask images
% ovlIcs -      ST stack of overlay of input image and mask of spots
% 
spots = [];
smaskIcs = [];
ovlIcs = [];
verbose = 1;
smaskIcs = newim(size(ics), 'bin');
seqLength = size(ics,3);
fprintf(['Detecting points. Overall ' num2str(seqLength-1) ' slices. Progress: ']);
tic;
if (verbose>0)
    figure;
end
for i=0:seqLength-1
    
    if (~isempty(cellm))
        cm = slice_ex(cellm,i);
    else
        cm = [];
    end
    img = slice_ex(ics,i);
    [pvec, smask] = algo_find_points_tresh(img, par.tr, cm, par.sizeTr, par.trMagn, par.openR, par.contSmoothSigma, par.contSmoothThreshold, par.sigma);

    spots{i+1} = pvec;
    smaskIcs = slice_in(smaskIcs, smask, i);
    if (verbose>0)
        imshow(uint8(overlay(stretch(slice_ex(ics,i), 0, 99.9), slice_ex(smaskIcs,i)-berosion(slice_ex(smaskIcs,i)), [255 0 0])));
        text(20,20,['Frame: ' int2str(i)], 'Color', 'White');
        drawnow
    end
    dispProgressCounter(i,0)
end
fprintf('\n');
toc;
if (nargout == 3)
    disp('Computing smask contour overlay...');tic;
    smaskIcsE = slice_op('berosion', smaskIcs, 1);
    smaskIcsC = smaskIcs-smaskIcsE;
    ovlIcs = overlay(ics, smaskIcsC, [255 0 0]);
    toc;
end

end