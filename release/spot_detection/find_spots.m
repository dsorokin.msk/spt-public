function [spots, smaskIcs, ovlIcs, l1ics] = find_spots(ics, cellm, sigma, sizeTr, trMagn, numOfLvl, localMax, verbose)
% finds spots in all frames of input ics stack
% 
% synopsys:
% [spots smaskIcs ovlIcs] = find_spots(ics, cellm, sigma, sizeTr, trMagn, numOfLvl, verbose)
% 
% inputs:
% ics -         input ST stack of images
% cellm -       ST stack of body mask of input images
% sigma -       sigma parameter for spot detection (used to compute Hessians)
% sizeTr -      threshold for spot size (only spots with size>sizeTr remain)
%               if sizeTr has 2 values, then spots with size>sizeTr(1) &
%               size<sizeTr(2) remain
% trMagn -      magnification of threshold found by triangle method. default
%               value is 1 (means using 'triangle' threshold
% numOfLvl -    number of levels in Hessian scale-space
% localMax -    if true - applyes local maxima to the result to get 1 pixel
%               points
% verbose -     verbose 1|0
% 
% outputs:
% spots -       cell array of coordinates of centroids of found spots
% smaskIcs -    ST stack of spots mask images
% ovlIcs -      ST stack of overlay of input image and mask of spots
% 

    if (nargout >= 3)
        ovlIcs = newimar(newim(size(ics)),newim(size(ics)),newim(size(ics)));
        ovlIcs = colorspace(ovlIcs, 'RGB');
    end
    
    if (nargin < 5)
        trMagn = 1;
    end
    
    if (nargin < 6)
        numOfLvl = 2;
    end
    
    if (nargin < 7)
        localMax = 0;
    end
    
    if (nargin < 8)
        verbose = 0;
    end
    
    smaskIcs = newim(size(ics), 'bin');
    smaskOvl = newim(size(ics), 'bin');
    l1ics = newim(size(ics));

    seqLength = size(ics,3);
    fprintf(['Detecting points. Overall ' num2str(seqLength-1) ' slices. Progress: ']);
    tic;
    if (verbose>0)
        figure;
    end
%     times = [];
    for i=0:seqLength-1
%         tic;
        if (~isempty(cellm))
            if size(cellm,3) > 1
                [pvec , ~, smask, l1] = algo_find_points1(slice_ex(ics,i), sigma, slice_ex(cellm,i), sizeTr, trMagn, numOfLvl, localMax);
            else
                [pvec , ~, smask, l1] = algo_find_points1(slice_ex(ics,i), sigma, cellm, sizeTr, trMagn, numOfLvl, localMax);
            end
        else
            [pvec , ~, smask, l1] = algo_find_points1(slice_ex(ics,i), sigma, [], sizeTr, trMagn, numOfLvl, localMax);
        end
        
%         [pvec , ~, smask] = algo_find_points_morph(slice_ex(ics,i), slice_ex(cellm,i), 4, 60);
%         t = toc;
%         times = [times; t];
        spots{i+1} = pvec;
        smaskIcs = slice_in(smaskIcs, smask, i);
        l1ics = slice_in(l1ics, l1, i);
        if (verbose>0)
            if ~localMax
                smaskOvl = slice_in(smaskOvl, slice_ex(smaskIcs,i)-berosion(slice_ex(smaskIcs,i)), i);
            else
                smaskOvl = slice_in(smaskOvl, bdilation(slice_ex(smaskIcs,i),1), i);
            end
%             imshow(uint8(overlay(stretch(slice_ex(ics,i), 0, 99.9), slice_ex(smaskOvl,i), [255 0 0])));
            imshow(uint8(overlay(stretch(slice_ex(l1ics,i), 0, 99.9), slice_ex(smaskOvl,i), [255 0 0])));
%             imshow(uint8(overlay(slice_ex(ics,i), slice_ex(smaskOvl,i), [255 0 0])));
            text(0,-20,['Frame: ' int2str(i)], 'Color', 'Black');
            drawnow
        end
        dispProgressCounter(i,0)
    end
%     mean(times)
    fprintf('\n');
    toc;
    if (nargout == 3)
        disp('Computing smask contour overlay...');tic;
%         if ~localMax
%             smaskIcsE = slice_op('berosion', smaskIcs, 1);
%             smaskIcsC = smaskIcs-smaskIcsE;
%         else
%             smaskIcsC = slice_op('bdilation', smaskIcs, 1);
%         end
        ovlIcs = overlay(ics, smaskOvl, [255 0 0]);
        toc;
    end
end