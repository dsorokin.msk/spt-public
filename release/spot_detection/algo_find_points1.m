function [pvec, pimg, smask, l1]  = algo_find_points1(img, sigma, cellM, sizeTr, trMagn, numOfLvl, localMax)
% finds spots in the given image
% 
% synopsys:
% [pvec pimg smask]  = algo_find_points1(img, sigma, cellM, sizeTr, trMagn, numOfLvl)
% 
% inputs:
% img -         input image
% sigma -       sigma for finest level in scale space of Hessians
%               (parameter for spot detection)
% cellM -       cell body mask, used to suppress spots outside of cell
%               body
% sizeTr -      threshold for spot size (only spots with size>sizeTr remain)
%               if sizeTr has 2 values, then spots with size>sizeTr(1) &
%               size<sizeTr(2) remain
% trMagn -      magnification of threshold found by triangle method. default
%               value is 1 (means using 'triangle' threshold)
% numOfLvl -    number of levels in Hessian scale-space
% localMax -    if true - applyes local maxima to the result to get 1 pixel
%               points
% 
% outputs:
% pvec -        coordinates of centroids of found spots and their areas
%               structure: [x y area]
% pimg -        binary image with points in coordinates of found spots
%               centroids
% smask -       binary image with mask of found spots
% 

% set sizeTr value for correct size filtering
if (length(sizeTr) < 2)
    sizeTr(2)=0;
end

% preprocess
b = stretch(img);

l1 = newim(imsize(img), 'double');
l1(:) = -inf;
for i=0:numOfLvl-1
    s = (2^i)*sigma;
    ev = eig(hessian(b, s));
    l2 = -ev{1};
    l1 = max(l1,s*s*l2);
end


if (sum(abs(l1))>1e-10)
    % find treshold by triangle method, >=min(l1(:)/2) is done to gaurantee 
    % the right tail of histogram is longer and thus to be chosen by triangle method 
    minl = -max(l1(:))*0.95;
    [~, tr] = threshold(l1(l1>=minl), 'triangle');
    % get the bins for histogram used in triangle method
    [~, bins] = diphist(l1, [minl max(l1(:))]);
    % shift the treshold to 4 bins to the right to ensure sorrect segmentation
    tr1 = tr + 4*(bins(2)-bins(1));
    % use the threshold modified by trMagn coefficient to ge the mask
    smask = threshold(l1, 'fixed', tr1*trMagn);
    smask = fillholes(smask);
    
    if (localMax)
        smask = maxima(l1,2)*smask;
    end
else
    smask = newim(imsize(l1), 'bin');
end
% multiply by cell body mask to remove outliers
if (~isempty(cellM))
    smask = cellM * smask;
end
if (localMax)
    pimg = smask;
    pvec = findcoord(smask);
    pvec = [pvec ones(size(pvec,1),1)];
else
    
    % filter by size
    smask = dip_image(label(smask, inf, sizeTr(1), sizeTr(2)), 'bin');
    % get centroids
    m = measure(label(smask), img, {'Gravity', 'Size'});
    % Create pointset
    if ~isempty(m)
        pvec = m.Gravity';
        pimg = coord2image(round(pvec), imsize(img));
        % add areas to pvec
        pvec = [pvec m.Size'];
    else
        pvec = [];
        pimg = newim(imsize(img));
    end
end

end