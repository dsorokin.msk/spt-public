function [res, ovlres] = plotPointsIcs(spots, imsize);

if (nargout > 1)
    ovlres = newimar(newim(imsize, 'bin'),newim(imsize,'bin'),newim(imsize,'bin'));
    ovlres = colorspace(ovlres, 'RGB');
end


if (nargout > 1)
    res1 = newim(imsize, 'bin');
end
szPlane = imsize(1:2);
seqLength = length(spots);
res = newim([szPlane seqLength], 'bin');

fprintf(['Drawing points... Overall ' num2str(seqLength-1) ' slices. Progress: ']);tic;
pp1 = plotPoints(spots{1}(:,1:2), szPlane);
for i=0:seqLength-1
    pp = plotPoints(spots{i+1}(:,1:2), szPlane);
    res = slice_in(res, pp, i);
    if (nargout > 1)
        res1 = slice_in(res1, pp1, i);
    end
    dispProgressCounter(i,0)
end
if (nargout > 1)
    ovlres = overlay(255*uint8(res1), res, [255 0 0]);   
end
fprintf('\n');
toc
clear res1

% fnameRes = [fname(1:end-4) '_tNRsh_pts.ics'];
% writeim(res, fnameRes);

end