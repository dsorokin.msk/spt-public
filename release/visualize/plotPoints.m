function img = plotPoints(P, imSize)
    P(isnan(P)) = 0;
    img = bdilation(coord2image((P), imSize),2);
end