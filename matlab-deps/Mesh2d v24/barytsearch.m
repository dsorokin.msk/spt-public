function ind = barytsearch(x,y,t,xi,yi)
% The replacement of function tsearch that was removed
% Implemented with Matlab routines, without use of .mex and C
% Can be further optimized using .mex and C
% 
% T = barytsearch(x,y,t,xi,yi) returns an index into the rows of TRI for
% each % point in xi, yi. The tsearch command returns NaN for all points 
% outside % the convex hull. Requires a triangulation TRI of the points 
% x,y obtained from triangulation.

x1 = [x(t(:,1)) y(t(:,1))]';
x2 = [x(t(:,2)) y(t(:,2))]';
x3 = [x(t(:,3)) y(t(:,3))]';
x = [xi yi]';

% x1 = [2 2; 3 4; 2 5; -2 -3]';
% x2 = [0 0; -1 -1; 1 1; -3 -1;]';
% x3 = [0 1; 1 0; 3 3; -2 -2]';
% x = [0.9 1; -0.9 1; 0.5 0.5]';

T = [x1-x3 x2-x3];
% first dimention - coord, second dimention - point, third dimention -
% triangle
T = permute(reshape(T, size(x1,1), size(x1,2), 2), [1 3 2]);
% first dimention - coord, second dimention - point, third dimention -
% triangle
Y = repmat(x, [1 1 size(T,3)]) - permute(repmat(x3, [1 1 size(x,2)]), [1 3 2]);
% find barycentric coordinates of x with respect to x1,x2,x3 for each
% triangle
l = cellfun(@(xxx,yyy) xxx\yyy, num2cell(T,[1 2]), num2cell(Y,[1 2]),'UniformOutput',false);
l = cat(3,l{:});
% find the third coordinate
l(3,:,:) = 1-sum(l,1);
% matrix with 1 where all the barycentric coordinates are in [0,1]
P_T = sum(l>=0-1e-15 & l<=1+1e-15,1) == 3;
P_T = permute(P_T, [2 3 1]);% columns are triangles, rows are points

[r,c] = find(P_T);
ind = zeros([1 size(x, 2)]);
if (sum(sum(P_T,2) > 1 ))
    [r,ii] =  unique(r, 'first');
    c = c(ii);
end
ind(r) = c;
% ind = ind';

DBG = 0;
if (DBG)
    figure;
    hold on
    for i=1:size(x1,2)
        xx = [x1(:,i) x2(:,i) x3(:,i) x1(:,i)];
        plot(xx(1,:), xx(2,:), 'o-r')
    end
    axis equal
    for i=1:size(x,2)
        plot(x(1,i), x(2,i), '*b')
    end
    for i=length(ind)
        tr_ind = ind(i);
        if (tr_ind == 0)
            plot(x(1,i), x(2,i), 'sqc')
            aaa1 = 1;
        end
        if (tr_ind > 0)
            xx = [x1(:,tr_ind) x2(:,tr_ind) x3(:,tr_ind) x1(:,tr_ind)];
        end
        plot(xx(1,:), xx(2,:), '--g')
        if (i > size(x,2))
            aaaa = 1;
        end
        plot(x(1,i), x(2,i), 'og')
    end
    % plot(x(1,ind), x(2,ind), 'og')
    hold off
end
    
end