## Introduction

The project contains the code for compensation for cell global motion and Single Particle Tracking and analysis.

## System requirements

The code runs on Windows 10 with Matlab 2018a.

## Installation

1. Install DipImage 2.9 (http://www.diplib.org/download).
2. Clone the repository to desired location.
3. Unpack the sofa-bin.zip.
4. Open release/cell_image_registration/registration_example.m 
5. Make sure the paths to SOFA binaries section are correct.
6. Run the registration_example.m script to calculate the deformation fields for the example image sequence.

## Dependencies

We acknowledge the use of the following software in this project:
* DipImage (http://www.diplib.org)
* SOFA (https://www.sofa-framework.org/)
* Mesh2d v.2.4 (https://github.com/dengwirda/mesh2d)

## References

In case of using the provided software please properly cite the following papers.

The complete description of the non-rigid cell image registration approach can be found in:

[1] Sorokin, D. V., Peterlik, I., Tektonidis, M., Rohr, K., & Matula, P. (2017). Non-Rigid Contour-Based Registration of Cell Nuclei in 2-D Live Cell Microscopy Images Using a Dynamic Elasticity Model. IEEE transactions on medical imaging, 37(1), 173-184.

The description of tracks analysis and motion type analysis can be found in:

[2] Arifulin, E. A., Sorokin, D. V., Tvorogova, A. V., Kurnaeva, M. A., Musinova, Y. R., Zhironkina, O. A., Golyshev, S. A., Abramchuk, S. S., Vassetzky, Y. S., & Sheval, E. V. (2018). Heterochromatin restricts the mobility of nuclear bodies. Chromosoma, 127(4), 529-537.

The description of spot detection can be found in:

[3] Foltánková, V., Matula, P., Sorokin, D., Kozubek, S., & Bártová, E. (2013). Hybrid detectors improved time-lapse confocal microscopy of PML and 53BP1 nuclear body colocalization in DNA lesions. Microscopy and Microanalysis, 19(2), 360-369.

## Authors
The following authors have contributed to the code:

Dmitry Sorokin, Laboratory of Mathematical Methods of Image Processing, Faculty of Computational Mathematics and Cybernetics, Lomonosov Moscow State University

Igor Peterlik, Inria, France

Pavel Matula, Centre for Biomedical Image Analysis, Faculty of Informatics, Masaryk University

Karl Rohr, University of Heidelberg, BIOQUANT Center, Faculty of BioScience, IPMB, and the DKFZ Heidelberg

Marco Tektonidis, University of Heidelberg, BIOQUANT Center, Faculty of BioScience, IPMB, and the DKFZ Heidelberg

## Contact information
dsorokin@cs.msu.ru